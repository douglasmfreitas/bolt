/*
 * Win32.cpp
 *
 *  Created on: 08/01/2013
 *      Author: Douglas
 */

#include <core/Win32.h>
#include <core/Window.h>
#include <core/Display.h>
#include <core/Input.h>

#include <boost/locale.hpp>
#include <GL/glew.h>

#define BOLT_CORE_WINDOW_WNDCLASSEX "BoltWindow"

UINT vkToSC(WPARAM wparam, LPARAM lparam) {
	if (HIWORD(lparam) & 0x0F00) {
		UINT scancode = MapVirtualKey(wparam, 0);
		return scancode | 0x80;
	} else {
		return HIWORD(lparam) & 0x00FF;
	}
}

namespace bolt {
	namespace core {
		Input::KeyCode Win32::keycode(size_t ivk) {
			static Input::KeyCode table[] = {
					Input::Null, //00
					Input::Null, //01
					Input::Null, //02
					Input::Null, //03
					Input::Null, //04
					Input::Null, //05
					Input::Null, //06
					Input::Null, //07
					Input::KBBackspace, //08
					Input::KBTab, //09
					Input::Null, //0a
					Input::Null, //0b
					Input::KBClear, //0c
					Input::KBEnter, //0d
					Input::Null, //0e
					Input::Null, //0f
					Input::KBLeftShift, //10
					Input::KBLeftCtrl, //11
					Input::KBLeftAlt, //12
					Input::KBBreak, //13
					Input::KBCapsLock, //14
					Input::Null, //15
					Input::Null, //16
					Input::Null, //17
					Input::Null, //18
					Input::Null, //19
					Input::Null, //1a
					Input::KBEsc, //1b
					Input::Null, //1c
					Input::Null, //1d
					Input::Null, //1e
					Input::Null, //1f
					Input::KBSpace, //20
					Input::KBPageUp, //21
					Input::KBPageDown, //22
					Input::KBEnd, //23
					Input::KBHome, //24
					Input::KBLeft, //25
					Input::KBUp, //26
					Input::KBRight, //27
					Input::KBDown, //28
					Input::Null, //29
					Input::Null, //2a
					Input::Null, //2b
					Input::KBPrintScreen, //2c
					Input::KBInsert, //2d
					Input::KBDelete, //2e
					Input::Null, //2f
					Input::KB0, //30
					Input::KB1, //31
					Input::KB2, //32
					Input::KB3, //33
					Input::KB4, //34
					Input::KB5, //35
					Input::KB6, //36
					Input::KB7, //37
					Input::KB8, //38
					Input::KB9, //39
					Input::Null, //3a
					Input::Null, //3b
					Input::Null, //3c
					Input::Null, //3d
					Input::Null, //3e
					Input::Null, //3f
					Input::Null, //40
					Input::KBA, //41
					Input::KBB, //42
					Input::KBC, //43
					Input::KBD, //44
					Input::KBE, //45
					Input::KBF, //46
					Input::KBG, //47
					Input::KBH, //48
					Input::KBI, //49
					Input::KBJ, //4a
					Input::KBK, //4b
					Input::KBL, //4c
					Input::KBM, //4d
					Input::KBN, //4e
					Input::KBO, //4f
					Input::KBP, //50
					Input::KBQ, //51
					Input::KBR, //52
					Input::KBS, //53
					Input::KBT, //54
					Input::KBU, //55
					Input::KBV, //56
					Input::KBW, //57
					Input::KBX, //58
					Input::KBY, //59
					Input::KBZ, //5a
					Input::KBLeftSuper, //5b
					Input::KBRightSuper, //5c
					Input::KBApplication, //5d
					Input::Null, //5e
					Input::KBSleep, //5f
					Input::KBNum0, //60
					Input::KBNum1, //61
					Input::KBNum2, //62
					Input::KBNum3, //63
					Input::KBNum4, //64
					Input::KBNum5, //65
					Input::KBNum6, //66
					Input::KBNum7, //67
					Input::KBNum8, //68
					Input::KBNum9, //69
					Input::KBNumMult, //6a
					Input::KBNumPlus, //6b
					Input::Null, //6c
					Input::KBNumMinus, //6d
					Input::KBNumDel, //6e
					Input::KBNumDiv, //6f
					Input::KBF1, //70
					Input::KBF2, //71
					Input::KBF3, //72
					Input::KBF4, //73
					Input::KBF5, //74
					Input::KBF6, //75
					Input::KBF7, //76
					Input::KBF8, //77
					Input::KBF9, //78
					Input::KBF10, //79
					Input::KBF11, //7a
					Input::KBF12, //7b
					Input::Null, //7c
					Input::Null, //7d
					Input::Null, //7e
					Input::Null, //7f
					Input::Null, //80
					Input::Null, //81
					Input::Null, //82
					Input::Null, //83
					Input::Null, //84
					Input::Null, //85
					Input::Null, //86
					Input::Null, //87
					Input::Null, //88
					Input::Null, //89
					Input::Null, //8a
					Input::Null, //8b
					Input::Null, //8c
					Input::Null, //8d
					Input::Null, //8e
					Input::Null, //8f
					Input::KBNumLock, //90
					Input::KBScrollLock, //91
					Input::Null, //92
					Input::Null, //93
					Input::Null, //94
					Input::Null, //95
					Input::Null, //96
					Input::Null, //97
					Input::Null, //98
					Input::Null, //99
					Input::Null, //9a
					Input::Null, //9b
					Input::Null, //9c
					Input::Null, //9d
					Input::Null, //9e
					Input::Null, //9f
					Input::KBLeftShift, //a0
					Input::KBRightShift, //a1
					Input::KBLeftCtrl, //a2
					Input::KBRightCtrl, //a3
					Input::KBLeftAlt, //a4
					Input::KBRightAlt, //a5
					Input::Null, //a6
					Input::Null, //a7
					Input::Null, //a8
					Input::Null, //a9
					Input::Null, //aa
					Input::Null, //ab
					Input::Null, //ac
					Input::Null, //ad
					Input::Null, //ae
					Input::Null, //af
					Input::Null, //b0
					Input::Null, //b1
					Input::Null, //b2
					Input::Null, //b3
					Input::Null, //b4
					Input::Null, //b5
					Input::Null, //b6
					Input::Null, //b7
					Input::Null, //b8
					Input::Null, //b9
					Input::KBCedille, //ba
					Input::KBEqual, //bb
					Input::KBComma, //bc
					Input::KBMinus, //bd
					Input::KBPeriod, //be
					Input::KBSemicolon, //bf
					Input::KBApostrophe, //c0
					Input::KBSlash, //c1
					Input::KBNumPeriod, //c2
					Input::Null, //c3
					Input::Null, //c4
					Input::Null, //c5
					Input::Null, //c6
					Input::Null, //c7
					Input::Null, //c8
					Input::Null, //c9
					Input::Null, //ca
					Input::Null, //cb
					Input::Null, //cc
					Input::Null, //cd
					Input::Null, //ce
					Input::Null, //cf
					Input::Null, //d0
					Input::Null, //d1
					Input::Null, //d2
					Input::Null, //d3
					Input::Null, //d4
					Input::Null, //d5
					Input::Null, //d6
					Input::Null, //d7
					Input::Null, //d8
					Input::Null, //d9
					Input::Null, //da
					Input::KBAgudo, //db
					Input::KBBracketClose, //dc
					Input::KBBracketOpen, //dd
					Input::KBTil, //de
					Input::Null, //df
					Input::Null, //e0
					Input::Null, //e1
					Input::KBBackslash, //e2
					Input::Null, //e3
					Input::Null, //e4
					Input::Null, //e5
					Input::Null, //e6
					Input::Null, //e7
					Input::Null, //e8
					Input::Null, //e9
					Input::Null, //ea
					Input::Null, //eb
					Input::Null, //ec
					Input::Null, //ed
					Input::Null, //ee
					Input::Null, //ef
					Input::Null, //f0
					Input::Null, //f1
					Input::Null, //f2
					Input::Null, //f3
					Input::Null, //f4
					Input::Null, //f5
					Input::Null, //f6
					Input::Null, //f7
					Input::Null, //f8
					Input::Null, //f9
					Input::Null, //fa
					Input::Null, //fb
					Input::Null, //fc
					Input::Null, //fd
					Input::Null //fe
			};

			return table[ivk];
		}

		size_t Win32::virtualkey(Input::KeyCode ikey) {
			switch(ikey) {
				case Input::Null: //07
					return 0x07;

				case Input::KBEsc: //0x1b
					return 0x1b;

				case Input::KBTab: //0x09
					return 0x09;
				case Input::KBEnter: //0x0d
					return 0x0d;
				case Input::KBSpace: //0x20
					return 0x20;
				case Input::KBBackspace: //0x08
					return 0x08;

				case Input::KBPageUp: //0x21
					return 0x21;
				case Input::KBPageDown: //0x22
					return 0x22;
				case Input::KBEnd: //0x23
					return 0x23;
				case Input::KBHome: //0x24
					return 0x24;
				case Input::KBInsert: //0x2d
					return 0x2d;
				case Input::KBDelete: //0x2e
					return 0x2e;
				case Input::KBClear: //0x0c
					return 0x0c;

				case Input::KBPrintScreen: //0x2c
					return 0x2c;
				case Input::KBBreak: //0x13
					return 0x13;
				case Input::KBSleep:
					return 0x5f;
				case Input::KBApplication:
					return 0x5d;

				case Input::KBCapsLock: //0x14
					return 0x14;
				case Input::KBNumLock: //0x90
					return 0x90;
				case Input::KBScrollLock: //0x91
					return 0x91;

				case Input::KBLeft: //0x25
					return 0x25;
				case Input::KBUp: //0x26
					return 0x26;
				case Input::KBRight: //0x27
					return 0x27;
				case Input::KBDown: //0x28
					return 0x28;

				case Input::KB0: //0x30
					return 0x30;
				case Input::KB1: //0x31
					return 0x31;
				case Input::KB2: //0x32
					return 0x32;
				case Input::KB3: //0x33
					return 0x33;
				case Input::KB4: //0x34
					return 0x34;
				case Input::KB5: //0x35
					return 0x35;
				case Input::KB6: //0x36
					return 0x36;
				case Input::KB7: //0x37
					return 0x37;
				case Input::KB8: //0x38
					return 0x38;
				case Input::KB9: //0x39
					return 0x39;

				case Input::KBA: //0x41
					return 0x41;
				case Input::KBB: //0x42
					return 0x42;
				case Input::KBC: //0x43
					return 0x43;
				case Input::KBD: //0x44
					return 0x44;
				case Input::KBE: //0x45
					return 0x45;
				case Input::KBF: //0x46
					return 0x46;
				case Input::KBG: //0x47
					return 0x47;
				case Input::KBH: //0x48
					return 0x48;
				case Input::KBI: //0x49
					return 0x49;
				case Input::KBJ: //0x4a
					return 0x4a;
				case Input::KBK: //0x4b
					return 0x4b;
				case Input::KBL: //0x4c
					return 0x4c;
				case Input::KBM: //0x4d
					return 0x4d;
				case Input::KBN: //0x4e
					return 0x4e;
				case Input::KBO: //0x4f
					return 0x4f;
				case Input::KBP: //0x50
					return 0x50;
				case Input::KBQ: //0x51
					return 0x51;
				case Input::KBR: //0x52
					return 0x52;
				case Input::KBS: //0x53
					return 0x53;
				case Input::KBT: //0x54
					return 0x54;
				case Input::KBU: //0x55
					return 0x55;
				case Input::KBV: //0x56
					return 0x56;
				case Input::KBW: //0x57
					return 0x57;
				case Input::KBX: //0x58
					return 0x58;
				case Input::KBY: //0x59
					return 0x59;
				case Input::KBZ: //0x5a
					return 0x5a;

				case Input::KBNum0: //0x60
					return 0x60;
				case Input::KBNum1: //0x61
					return 0x61;
				case Input::KBNum2: //0x62
					return 0x62;
				case Input::KBNum3: //0x63
					return 0x63;
				case Input::KBNum4: //0x64
					return 0x64;
				case Input::KBNum5: //0x65
					return 0x65;
				case Input::KBNum6: //0x66
					return 0x66;
				case Input::KBNum7: //0x67
					return 0x67;
				case Input::KBNum8: //0x68
					return 0x68;
				case Input::KBNum9: //0x69
					return 0x69;
				case Input::KBNumMult: //0x6a
					return 0x6a;
				case Input::KBNumPlus: //0x6b
					return 0x6b;
				case Input::KBNumMinus: //0x6d
					return 0x6d;
				case Input::KBNumDel: //0x6e
					return 0x6e;
				case Input::KBNumDiv: //0x6f
					return 0x6f;
				case Input::KBNumPeriod: //0xc2
					return 0xc2;

				case Input::KBF1: //0x70
					return 0x70;
				case Input::KBF2: //0x71
					return 0x71;
				case Input::KBF3: //0x72
					return 0x72;
				case Input::KBF4: //0x73
					return 0x73;
				case Input::KBF5: //0x74
					return 0x74;
				case Input::KBF6: //0x75
					return 0x75;
				case Input::KBF7: //0x76
					return 0x76;
				case Input::KBF8: //0x77
					return 0x77;
				case Input::KBF9: //0x78
					return 0x78;
				case Input::KBF10: //0x79
					return 0x79;
				case Input::KBF11: //0x7a
					return 0x7a;
				case Input::KBF12: //0x7b
					return 0x7b;

				case Input::KBLeftShift: //0xa0
					return 0xa0;
				case Input::KBRightShift: //0xa1
					return 0xa1;
				case Input::KBLeftCtrl: //0xa2
					return 0xa2;
				case Input::KBRightCtrl: //0xa3
					return 0xa3;
				case Input::KBLeftAlt: //0xa4
					return 0xa4;
				case Input::KBRightAlt: //0xa5
					return 0xa5;
				case Input::KBLeftSuper:
					return 0x5b;
				case Input::KBRightSuper:
					return 0x5c;

				case Input::KBCedille: //0xba
					return 0xba;
				case Input::KBEqual: //0xbb
					return 0xbb;
				case Input::KBComma: //0xbc
					return 0xbc;
				case Input::KBMinus: //0xbd
					return 0xbd;
				case Input::KBPeriod: //0xbe
					return 0xbe;
				case Input::KBSemicolon: //0xbf
					return 0xbf;
				case Input::KBApostrophe: //0xc0
					return 0xc0;
				case Input::KBAgudo: //0xdb
					return 0xdb;
				case Input::KBBracketClose: //0xdc
					return 0xdc;
				case Input::KBBracketOpen: //0xdd
					return 0xdd;
				case Input::KBTil: //0xde
					return 0xde;
				case Input::KBSlash: //0xc1
					return 0xc1;
				case Input::KBBackslash: //0xe2
					return 0xe2;

					//Mouse
				case Input::MouseHu:
					return 0x07;
				case Input::MouseHd:
					return 0x07;
				case Input::MouseVu:
					return 0x07;
				case Input::MouseVd:
					return 0x07;
				case Input::MouseMu:
					return 0x07;
				case Input::MouseMd:
					return 0x07;

				case Input::MouseLeft:
					return 0x07;
				case Input::MouseRight:
					return 0x07;
				case Input::MouseMiddle:
					return 0x07;
				case Input::MouseX1:
					return 0x07;
				case Input::MouseX2:
					return 0x07;
				case Input::MouseX3:
					return 0x07;
				case Input::MouseX4:
					return 0x07;
			}
			return 0x07;
		}

		typedef std::pair<unsigned int, unsigned int> unitext;
		unitext translateWin32Text(unsigned int vk) {
			static WCHAR dead_key = 0;

			// Convert a non-combining diacritical mark into a combining diacritical mark
			// Combining versions range from 0x300 to 0x36F; only 5 (for French) have been mapped below
			// http://www.fileformat.info/info/unicode/block/combining_diacritical_marks/images.htm
			std::map<unsigned int, unsigned int> deadmap;
			deadmap[0x5E] = 0x302;            // Circumflex accent: �
			deadmap[0x60] = 0x300;            // Grave accent: �
			deadmap[0x7E] = 0x303;            // Tilde: �
			deadmap[0xA8] = 0x308;            // Diaeresis: �
			deadmap[0xB4] = 0x301;            // Acute accent: �
			deadmap[0xB8] = 0x327;            // Cedilla: �

			BYTE keyState[256];
			HKL layout = GetKeyboardLayout(0);
			if(GetKeyboardState(keyState) == 0) {
				return unitext(0, 0);
			}

			int code = MapVirtualKeyEx((UINT) vk, 4, layout);
			vk = MapVirtualKeyEx((UINT) code, 3, layout);
			if(vk == 0) {
				return unitext(0, 0);
			}

			WCHAR buff[3] = { 0, 0, 0 };
			int ascii = ToUnicodeEx(vk, (UINT) code, keyState, buff, 3, 0,
					layout);

			if(ascii == 1 && dead_key) {
				// A dead key is stored and we have just converted a character key
				// Combine the two into a single character
				WCHAR wc_buff[3] = { buff[0], (WCHAR) deadmap[dead_key], '\0' };
				WCHAR out[3];

				if(FoldStringW(MAP_PRECOMPOSED, (LPWSTR) wc_buff, 3,
						(LPWSTR) out, 3)) {
					if(out[0] == ' ') {
						unitext ret(dead_key, 0);
						dead_key = 0;
						return ret;
					}

					dead_key = 0;
					return unitext(out[0], out[1]);
				}
			} else if(ascii == 1) {
				return unitext(buff[0], 0);
			} else if(ascii >= 2 || ascii < 0) {
				if(dead_key) {
					unitext ret(dead_key, buff[0]);
					dead_key = 0;
					return ret;
				}

				if(deadmap.find(buff[0]) != deadmap.end()) {
					dead_key = buff[0];
				}
			}

			return unitext(0, 0);
		}

		POINT Win32::clientCenter(HWND iwindow_id) {
			RECT rect;
			GetClientRect(iwindow_id, &rect);

			POINT p = {rect.right/2, rect.bottom/2};
			return p;
		}

		POINT Win32::screenCenter(HWND iwindow_id) {
			POINT p = {0,0};
			RECT rect;

			ClientToScreen(iwindow_id, &p);
			GetClientRect(iwindow_id, &rect);

			p.x += rect.right/2;
			p.y += rect.bottom/2;

			return p;
		}

		size_t Win32::createWindow(std::string ititle, int ipos_x, int ipos_y, size_t iwidth, size_t iheight, Display *idisplay, bool icenter, bool iborders) {
			WNDCLASSEX window_class;
			window_class.cbSize			= sizeof(WNDCLASSEX);
			window_class.style			= CS_OWNDC;
			window_class.lpfnWndProc	= Win32::WindowProc;
			window_class.cbClsExtra		= 0;
			window_class.cbWndExtra		= 0;
			window_class.hInstance		= GetModuleHandle(NULL);
			window_class.hIcon			= NULL;
			window_class.hCursor		= LoadCursor(NULL, IDC_ARROW);
			window_class.hbrBackground	= (HBRUSH) GetStockObject(BLACK_BRUSH);
			window_class.lpszMenuName	= NULL;
			window_class.lpszClassName	= BOLT_CORE_WINDOW_WNDCLASSEX;
			window_class.hIconSm		= NULL;

			if(!RegisterClassEx(&window_class)) {
				throw CoreException("Win32::createWindow() : Não foi possivel registrar a classe de janela.");
			}

			DWORD style_ex = 0;
			DWORD style = 0;

			RECT window_rectangle;
			window_rectangle.left   = (long)0;
			window_rectangle.top    = (long)0;
			window_rectangle.right  = (long)iwidth;
			window_rectangle.bottom = (long)iheight;

			if(iborders) {
				style_ex = WS_EX_TOPMOST;
				style = WS_POPUP;
//				style_ex = WS_EX_APPWINDOW;
//				style = WS_POPUP;
			} else {
				style_ex = 0;
				style = WS_OVERLAPPEDWINDOW;
//				style_ex = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
//				style = WS_OVERLAPPEDWINDOW;
			}

			AdjustWindowRectEx(&window_rectangle, style, FALSE, style_ex);

			if(icenter) {
				idisplay->update();
				ipos_x = idisplay->mode(Display::DEFAULT).width()/2-(window_rectangle.right - window_rectangle.left)/2;
				ipos_y = idisplay->mode(Display::DEFAULT).height()/2-(window_rectangle.bottom - window_rectangle.top)/2;
			}

			HWND window = CreateWindowEx(
				style_ex,
				BOLT_CORE_WINDOW_WNDCLASSEX,
				base::sysEncode(ititle).c_str(),
				style,
				ipos_x, ipos_y,
				window_rectangle.right-window_rectangle.left,
				window_rectangle.bottom-window_rectangle.top,
				NULL, NULL, GetModuleHandleW(NULL), NULL
			);

			if(!window) {
				throw CoreException(base::print("Win32::createWindow() : Não foi possivel criar a janela: %d", GetLastError()));
			}

			return (size_t)window;
		}

		void Win32::destroyWindow(size_t iwindow_id) {
			if(!DestroyWindow((HWND)iwindow_id)) {
				throw CoreException("Win32::destroyWindow() : Não foi possivel fechar a janela.");
			}

			if(!UnregisterClass(BOLT_CORE_WINDOW_WNDCLASSEX,GetModuleHandle(NULL))) {
				throw CoreException("Win32::createWindow() : Não foi possivel desregistrar a classe de janela.");
			}
		}

		void Win32::createOpenGL(size_t iwindow_id, void **odevice_context, void **oopengl_context) {
			(*odevice_context) = GetDC((HWND)iwindow_id);

			PIXELFORMATDESCRIPTOR pixel_format;
			memset(&pixel_format, 0, sizeof(pixel_format));

			pixel_format.nSize = sizeof(pixel_format);
			pixel_format.nVersion = 1;
			pixel_format.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
			pixel_format.iPixelType = PFD_TYPE_RGBA;
			pixel_format.cColorBits = 32;
			pixel_format.cDepthBits = 16;
			//pixel_format.iLayerType = PFD_MAIN_PLANE;

			SetPixelFormat((HDC)(*odevice_context), ChoosePixelFormat((HDC)(*odevice_context), &pixel_format), &pixel_format);

			(*oopengl_context) = wglCreateContext((HDC)(*odevice_context));
			wglMakeCurrent((HDC)(*odevice_context), (HGLRC)(*oopengl_context));

			if(glewInit() == GLEW_OK) {
				if(!GLEW_VERSION_3_0) {
					throw CoreException("Win32::createOpenGL() : OpenGL 3.0 não disponivel.");
				} else {
					base::log(base::Nothing, "Win32::createOpenGL() : OpenGL 3.0 disponivel.");
				}
			} else {
				throw CoreException("Win32::createOpenGL() : Não foi possivel inicializar a GLEW.");
			}

			glClearColor(0.2, 0.1, 0.4, 1.0);
			glEnable(GL_DEPTH_TEST);
		}

		void Win32::destroyOpenGL(size_t iwindow_id, void *idevice_context, void *iopengl_context) {
			wglMakeCurrent(NULL, NULL);
			wglDeleteContext((HGLRC)idevice_context);
			ReleaseDC((HWND)iwindow_id, (HDC)iopengl_context);
		}

		void Win32::showWindow(size_t iwindow_id, bool imaximized) {
			ShowWindow((HWND)iwindow_id,(imaximized?SW_SHOWMAXIMIZED:SW_SHOW));
			//SetForegroundWindow((HWND)iwindow_id);
			SetFocus((HWND)iwindow_id);
		}

		void Win32::setBorders(size_t iwindow_id, bool iborders) {
			DWORD style_ex = 0;
			DWORD style = 0;

			if(iborders) {
				style_ex = WS_EX_TOPMOST;
				style = WS_POPUP;
//				style_ex = WS_EX_APPWINDOW;
//				style = WS_POPUP;
			} else {
				style_ex = 0;
				style = WS_OVERLAPPEDWINDOW;
//				style_ex = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
//				style = WS_OVERLAPPEDWINDOW;
			}

			SetWindowLong((HWND)iwindow_id, GWL_STYLE, style);
			SetWindowLong((HWND)iwindow_id, GWL_EXSTYLE, style_ex);
		}

		void Win32::showCursor(bool ishow) {
			if(ishow) {
				while(ShowCursor(TRUE)<0);
			} else {
				while(ShowCursor(FALSE)>=0);
			}
		}

		void Win32::clipCursor(size_t iwindow_id, bool iclip) {
			if(iclip) {
				POINT p = {0, 0};
				ClientToScreen((HWND)iwindow_id, &p);
				RECT rect;
				GetClientRect((HWND)iwindow_id, &rect);
				rect.left += p.x;
				rect.right += p.x;
				rect.top += p.y;
				rect.bottom += p.y;
				ClipCursor(&rect);
			} else {
				ClipCursor(NULL);
			}
		}

		void Win32::setProp(size_t iwindow_id, std::string iname, void *prop) {
			if(!SetProp((HWND)iwindow_id, iname.c_str(), prop)) {
				throw CoreException(base::print("Win32::setProp : Não foi possivel criar a propriedade %s.", iname.c_str()));
			}
		}

		void *Win32::getProp(size_t iwindow_id, std::string iname) {
			void *prop;
			if(!(prop = GetProp((HWND)iwindow_id, iname.c_str()))) {
				throw CoreException(base::print("Win32::getProp : Não foi possivel obter a propriedade %s.", iname.c_str()));
			}
			return prop;
		}

		void Win32::storeWindowPlacement(size_t iwindow_id) {
			GetWindowPlacement((HWND)iwindow_id, (WINDOWPLACEMENT*)Win32::getProp(iwindow_id, "bolt::core::Window.placement"));
		}

		void Win32::restoreWindowPlacement(size_t iwindow_id) {
			SetWindowPlacement((HWND)iwindow_id, (WINDOWPLACEMENT*)Win32::getProp(iwindow_id, "bolt::core::Window.placement"));
		}

		LRESULT CALLBACK Win32::WindowProc(HWND iwindow_handler, UINT imessage, WPARAM iwparam, LPARAM ilparam) {
			Window *window = Window::get();
			if(window) {
				switch(imessage) {
					//Eventos de entrada do usuário
					case WM_HOTKEY:
						if((int)iwparam == window->_fullscreen_hotkey && window->_active) {
							base::log(base::Nothing, "Win32::WindowProc() : Mensagem recebida: WM_HOTKEY. Atalho do teclado: fullscreen.");
							window->fullscreen(!window->_fullscreen);
							return 0;
						}
						return 0;

					case WM_KEYDOWN: {
							Input::KeyCode key = keycode(iwparam);
							window->_input->onKeyDown(key);
							if(key == Input::Null) {
								base::log(base::Warning, "Win32::WindowProc() : Tecla não mapeada: 0x%04p", iwparam);
							}

							if(key == Input::KBF1) {
								window->holdCursor(true);
							} else if(key == Input::KBF2) {
								window->holdCursor(false);
							}

							unitext txt = translateWin32Text(iwparam);
							if(txt.first) {
								window->_input->onChar(txt.first);
							}
							if(txt.second) {
								window->_input->onChar(txt.second);
							}
						} return 0;
					case WM_KEYUP: {
							Input::KeyCode key = keycode(iwparam);
							window->_input->onKeyUp(key);
							if(key == Input::Null) {
								base::log(base::Warning, "Win32::WindowProc() : Tecla não mapeada: 0x%04p", iwparam);
							}
						} return 0;

					case WM_MOUSEMOVE: {
							POINT mouse = {GET_X_LPARAM(ilparam), GET_Y_LPARAM(ilparam)};
							if(window->_cursor_held) {
								POINT center = Win32::clientCenter((HWND)window->_window_id);
								if(mouse.x != center.x || mouse.y != center.y) {
									mouse.x = mouse.x-center.x;
									mouse.y = mouse.y-center.y;

									window->_input->onKeyMove(Input::MouseHu, (float)(mouse.x>0?mouse.x:0));
									window->_input->onKeyMove(Input::MouseHd, (float)(mouse.x<0?-mouse.x:0));
									window->_input->onKeyMove(Input::MouseVu, (float)(mouse.y>0?mouse.y:0));
									window->_input->onKeyMove(Input::MouseVd, (float)(mouse.y<0?-mouse.y:0));
								}
							}
						} return 0;

					case WM_LBUTTONDOWN:
						window->_input->onKeyDown(Input::MouseLeft);
						return 0;
					case WM_LBUTTONUP:
						window->_input->onKeyUp(Input::MouseLeft);
						return 0;

					case WM_RBUTTONDOWN:
						window->_input->onKeyDown(Input::MouseRight);
						return 0;
					case WM_RBUTTONUP:
						window->_input->onKeyUp(Input::MouseRight);
						return 0;

					case WM_MBUTTONDOWN:
						window->_input->onKeyDown(Input::MouseMiddle);
						return 0;
					case WM_MBUTTONUP:
						window->_input->onKeyUp(Input::MouseMiddle);
						return 0;

					case WM_MOUSEWHEEL: {
							float pos = (static_cast<float>((short)HIWORD(iwparam)) / static_cast<float>(120));
							window->_input->onKeyMove(Input::MouseMu, (pos>0?pos:0));
							window->_input->onKeyMove(Input::MouseMd, (pos<0?-pos:0));
						} return 0;

					case WM_XBUTTONDOWN: {
							switch(HIWORD(iwparam)) {
								case XBUTTON1:
									window->_input->onKeyDown(Input::MouseX1);
									break;
								case XBUTTON2:
									window->_input->onKeyDown(Input::MouseX2);
									break;
								default:
									base::log(base::Warning, "Win32::WindowProc() : Mensagem WM_XBUTTONDOWN recebida com um botão inesperado: %04p.", HIWORD(ilparam));
									break;
							}
						} return 0;
					case WM_XBUTTONUP: {
							switch(HIWORD(iwparam)) {
								case XBUTTON1:
									window->_input->onKeyUp(Input::MouseX1);
									break;
								case XBUTTON2:
									window->_input->onKeyUp(Input::MouseX2);
									break;
								default:
									base::log(base::Warning, "Win32::WindowProc() : Mensagem WM_XBUTTONUP recebida com um botão inesperado: %04p.", HIWORD(ilparam));
									break;
							}
						} return 0;

					//Eventos relacionados à janela
					case WM_ACTIVATE:
						if(iwparam == WA_INACTIVE) {
							base::log(base::Nothing, "Win32::WindowProc() : Mensagem recebida: WM_ACTIVATE. Janela desativada.");
							window->setActive(false);
						} else {
							base::log(base::Nothing, "Win32::WindowProc() : Mensagem recebida: WM_ACTIVATE. Janela ativada.");
							window->setActive(true);
						}
						return 0;
					case WM_CLOSE:
						base::log(base::Nothing, "Win32::WindowProc() : Mensagem recebida: WM_CLOSE. Janela fechada.");
						PostQuitMessage(0);
						return 0;
					case WM_SIZE:
						window->onResize(LOWORD(ilparam), HIWORD(ilparam));
						return 0;

					//Eventos do sistema
					case WM_NCDESTROY:
						base::log(base::Nothing, "Win32::WindowProc() : Mensagem recebida: WM_NCDESTROY. Janela destruida.");

						//Remover todas as propriedades adicionadas.
						RemoveProp(iwindow_handler, "bolt::core::Window");

						delete (WINDOWPLACEMENT*)GetProp(iwindow_handler, "bolt::core::Window.placement");
						RemoveProp(iwindow_handler, "bolt::core::Window.placement");
						return 0;
				}
			} else {
				switch(imessage) {
					//Mensagens recebidas durante a criação da janela.
					case WM_CREATE:
						base::log(base::Nothing, "Win32::WindowProc() : Mensagem recebida: WM_CREATE.");
						break;
					case WM_NCCREATE:
						base::log(base::Nothing, "Win32::WindowProc() : Mensagem recebida: WM_NCCREATE.");
						break;
					case WM_NCCALCSIZE:
						base::log(base::Nothing, "Win32::WindowProc() : Mensagem recebida: WM_NCCALCSIZE.");
						break;
					case WM_GETMINMAXINFO:
						base::log(base::Nothing, "Win32::WindowProc() : Mensagem recebida: WM_GETMINMAXINFO.");
						break;
					default:
						base::log(base::Warning, "Win32::WindowProc() : Mensagem inesperada recebida: %d.", imessage);
						break;
				}
			}

			return DefWindowProc(iwindow_handler,imessage,iwparam,ilparam);
		}
	}  // namespace core
}  // namespace bolt


