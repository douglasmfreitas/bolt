/*
 * InputText.cpp
 *
 *  Created on: 20/01/2013
 *      Author: Douglas
 */

#include <input/InputText.h>

#include <boost/locale.hpp>

namespace bolt {
	namespace input {
		std::string InputText::text() {
			return boost::locale::conv::utf_to_utf<char, unsigned int>(this->_text.c_str());
		}

		void InputText::text(std::string itext) {
			this->_text = boost::locale::conv::utf_to_utf<unsigned int>(itext.c_str());
		}

		void InputText::clear() {
			this->_text.clear();
		}

		void InputText::activate() {
			core::Input::get()->bind(this);
		}

		void InputText::deactivate() {
			core::Input::get()->unbind(this);
		}
	}  // namespace input
}  // namespace bolt
