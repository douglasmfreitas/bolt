/*
 * Vector.h
 *
 *  Created on: 30/01/2013
 *      Author: Douglas
 */

#ifndef BOLT_BASE_MATH_H_
#define BOLT_BASE_MATH_H_

#include <base/Base.h>

#include <cmath>

namespace bolt {
	namespace base {
		template<typename T>
		class Vector2 {
		private:
			T _x;
			T _y;

		public:
			Vector2():_x(0), _y(0){}
			Vector2(T ix, T iy):_x(ix), _y(iy) {}
			Vector2(const Vector2 &iv):_x(iv._x), _y(iv._y) {}

			T x() const {return this->_x;}
			T y() const {return this->_y;}

			void x(T ix) {this->_x = ix;}
			void y(T iy) {this->_y = iy;}

			T norm() const {return sqrt(_x*_x + _y*_y);}

			Vector2 &operator=(const Vector2 &iv) {
				_x = iv._x;
				_y = iv._y;
				return (*this);
			}

			Vector2	operator+(const Vector2 &iv)	const {return Vector2(iv._x+_x, iv._y+_y);}
			Vector2	operator-(const Vector2 &iv)	const {return Vector2(iv._x-_x, iv._y-_y);}
			T		operator*(const Vector2 &iv)	const {return iv._x*_x + iv._y*_y;}
			Vector2	operator*(const T &iv)			const {return Vector2(iv*_x, iv*_y);}
			Vector2	operator/(const T &iv)			const {return Vector2(_x/iv, _y/iv);}

			bool	operator==(const Vector2 &iv)	const {return iv._x == _x && iv._y == _y;}
			bool	operator!=(const Vector2 &iv)	const {return iv._x != _x || iv._y != _y;}
		};

		template<typename T>
		class Vector3 {
		private:
			T _x;
			T _y;
			T _z;

		public:
			Vector3():_x(0), _y(0), _z(0){}
			Vector3(T ix, T iy, T iz):_x(ix), _y(iy), _z(iz) {}
			Vector3(const Vector3 &iv):_x(iv._x), _y(iv._y), _z(iv._z) {}

			T x() const {return this->_x;}
			T y() const {return this->_y;}
			T z() const {return this->_z;}

			void x(T ix) {this->_x = ix;}
			void y(T iy) {this->_y = iy;}
			void z(T iz) {this->_z = iz;}

			T norm() const {return sqrt(_x*_x + _y*_y + _z*_z);}

			Vector3 &operator=(const Vector3 &iv) {
				_x = iv._x;
				_y = iv._y;
				_z = iv._z;
				return (*this);
			}

			Vector3	operator+(const Vector3 &iv)	const {return Vector3(iv._x+_x, iv._y+_y, iv._z+_z);}
			Vector3	operator-(const Vector3 &iv)	const {return Vector3(iv._x-_x, iv._y-_y, iv._z-_z);}
			T		operator*(const Vector3 &iv)	const {return iv._x*_x + iv._y*_y + iv._z*_z;}
			Vector3	operator*(const T &iv)			const {return Vector3(iv*_x, iv*_y, iv*_z);}
			Vector3	operator/(const T &iv)			const {return Vector3(_x/iv, _y/iv, _z/iv);}

			bool	operator==(const Vector3 &iv)	const {return iv._x == _x && iv._y == _y && iv._z == _z;}
			bool	operator!=(const Vector3 &iv)	const {return iv._x != _x || iv._y != _y || iv._z != _z;}
		};

	}  // namespace base
}  // namespace bolt

#endif /* BOLT_BASE_MATH_H_ */
