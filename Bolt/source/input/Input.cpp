/*
 * Input.cpp
 *
 *  Created on: 20/01/2013
 *      Author: Douglas
 */

#include <input/Input.h>

#include <input/InputChart.h>
#include <input/InputText.h>

namespace bolt {
	namespace input {
		Input::Input()
		:_text_input(new InputText()){
			this->addInputChart("menu", "Menu", "Comandos referentes ao menu do jogo.");

			this->_input_charts["menu"]->addCommand("up", "Cima","Move o cursor para cima.");
			this->_input_charts["menu"]->addCommand("down", "Baixo","Move o cursor para baixo.");
			this->_input_charts["menu"]->addCommand("right", "Direita","Move o cursor para a direita.");
			this->_input_charts["menu"]->addCommand("left", "Esquerda","Move o cursor para a esquerda.");

			this->_input_charts["menu"]->addCommand("select", "Selecionar","Seleciona o item indicado pelo cursor.");
			this->_input_charts["menu"]->addCommand("back", "Voltar/Cancelar","Volta para a página anterior ou cancela uma ação.");

			this->_input_charts["menu"]->bind(core::Input::KBUp, "up", 0);
			this->_input_charts["menu"]->bind(core::Input::KBDown, "down", 0);
			this->_input_charts["menu"]->bind(core::Input::KBRight, "right", 0);
			this->_input_charts["menu"]->bind(core::Input::KBLeft, "left", 0);

			this->_input_charts["menu"]->bind(core::Input::KBEnter, "select", 0);
			this->_input_charts["menu"]->bind(core::Input::KBEsc, "back", 0);

			this->addInputChart("character", "Personagem", "Comandos referentes ao controle do personagem.");

			this->_input_charts["character"]->addCommand("forth", "Frente","Move o personagem para frente.");
			this->_input_charts["character"]->addCommand("back", "Traz","Move o personagem para traz.");
			this->_input_charts["character"]->addCommand("right", "Direita","Move o personagem para a direita.");
			this->_input_charts["character"]->addCommand("left", "Esquerda","Move o personagem para a esquerda.");

			this->_input_charts["character"]->addCommand("tleft", "Girar p/ Esquerda","Gira a camera para a esquerda.");
			this->_input_charts["character"]->addCommand("tright", "Girar p/ Direita","Gira a camera para a direita.");
			this->_input_charts["character"]->addCommand("tup", "Girar p/ Cima","Gira a camera para cima.");
			this->_input_charts["character"]->addCommand("tdown", "Girar p/ Baixo","Gira a camera para baixo.");

			this->_input_charts["character"]->addCommand("act1", "Ação 1","Ativa a ação principal do personagem.");
			this->_input_charts["character"]->addCommand("act2", "Ação 2","Ativa a ação secundaria do personagem.");

			this->_input_charts["character"]->addCommand("run", "Correr","Faz com que o personagem corra.");
			this->_input_charts["character"]->addCommand("sneak", "Espreitar","Faz com que o personagem espreite.");

			this->_input_charts["character"]->bind(core::Input::KBUp, "forth", 0);
			this->_input_charts["character"]->bind(core::Input::KBDown, "back", 0);
			this->_input_charts["character"]->bind(core::Input::KBRight, "right", 0);
			this->_input_charts["character"]->bind(core::Input::KBLeft, "left", 0);

			this->_input_charts["character"]->bind(core::Input::KBW, "forth", 0);
			this->_input_charts["character"]->bind(core::Input::KBS, "back", 0);
			this->_input_charts["character"]->bind(core::Input::KBD, "right", 0);
			this->_input_charts["character"]->bind(core::Input::KBA, "left", 0);

			this->_input_charts["character"]->bind(core::Input::MouseHd, "tleft", 0);
			this->_input_charts["character"]->bind(core::Input::MouseHu, "tright", 0);
			this->_input_charts["character"]->bind(core::Input::MouseVd, "tup", 0);
			this->_input_charts["character"]->bind(core::Input::MouseMu, "tdown", 0);

			this->_input_charts["character"]->bind(core::Input::MouseLeft, "act1", 0);
			this->_input_charts["character"]->bind(core::Input::MouseRight, "act2", 0);

			this->_input_charts["character"]->bind(core::Input::KBLeftShift, "run", 0);
			this->_input_charts["character"]->bind(core::Input::KBLeftCtrl, "sneak", 0);
		}
		Input::~Input() {
			ChartMap::iterator it;
			for(it=this->_input_charts.begin() ; it!=this->_input_charts.end() ; it++) {
				delete it->second;
			}
			delete this->_text_input;
		}

		void Input::addInputChart(std::string iid, std::string iname, std::string idesc) {
			if(this->_input_charts.find(iid) != this->_input_charts.end()) {
				throw InputException(base::print("Input::addInputChart() : Tentou criar uma tabela com um identificador já existente: \"%s\".", iid.c_str()));
			}

			InputChart *inputchart = new InputChart(iname, idesc);
			inputchart->activate();

			this->_input_charts.insert(ChartMap::value_type(iid, inputchart));
		}

	}  // namespace input
}  // namespace bolt

