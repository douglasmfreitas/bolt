/*
 * Texture.h
 *
 *  Created on: 20/02/2013
 *      Author: Douglas M. Freitas
 */

#ifndef BOLT_RENDER_TEXTURE_H_
#define BOLT_RENDER_TEXTURE_H_

#include <render/Render.h>

namespace bolt {
	namespace render {
		class Texture {
		private:
			size_t _id;
			size_t _type;

			size_t _width;
			size_t _height;

		public:
			Texture():_id(0), _type(0), _width(0), _height(0) {}
			Texture(size_t iwidth, size_t iheight);
			Texture(const char *ifilename);
			virtual ~Texture();

			void loadBlank(size_t iwidth, size_t iheight);
			void loadImage(const char *ifilename);

			void loadColorBuffer(size_t iwidth, size_t iheight);
			void loadDepthBuffer(size_t iwidth, size_t iheight);

			void unload();

			void bind() const;

			size_t id() const {return _id;}
			size_t type() const {return _type;}

			size_t width() const {return _width;}
			size_t height() const {return _height;}
		};
	}  // namespace render
}  // namespace bolt

#endif /* BOLT_RENDER_TEXTURE_H_ */
