/*
 * Shader.h
 *
 *  Created on: 22/02/2013
 *      Author: Douglas M. Freitas
 */

#ifndef BOLT_RENDER_SHADER_H_
#define BOLT_RENDER_SHADER_H_

#include <render/Render.h>

namespace bolt {
	namespace render {
		class Shader {
		private:
			size_t _program_id;
			size_t _vert_id;
			size_t _frag_id;

			void log(std::string ivert, std::string ifrag);

		public:
			Shader(std::string ivert, std::string ifrag);
			~Shader();

			void bind();
			void unbind();

			void load(std::string ivert, std::string ifrag);
			void unload();
		};
	}  // namespace render
}  // namespace bolt

#endif /* BOLT_RENDER_SHADER_H_ */
