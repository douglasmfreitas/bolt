/*
 * InputText.h
 *
 *  Created on: 20/01/2013
 *      Author: Douglas
 */

#ifndef BOLT_INPUT_INPUTTEXT_H_
#define BOLT_INPUT_INPUTTEXT_H_

#include <input/Input.h>
#include <core/Input.h>

#include <vector>

namespace bolt {
	namespace input {
		/**
		 * Classe responsavel por obter a entrada de texto. Ela recebe os eventos de entrada de caracteres e os agrupa
		 * em uma string. Essa string é editada de acordo com os eventos de teclas recebidas da classe bolt::core::Input.
		 *
		 * @author Douglas M. Freitas
		 */
		class InputText : public core::Input::Listener {
		private:
			///Texto digitado.
			std::basic_string<unsigned int> _text;

			/**
			 * Função chamada toda vez que uma tecla é pressionada.
			 *
			 * @param ikey Tecla pressionada.
			 */
			virtual void onKeyDown(core::Input::KeyCode ikey) {
				if(core::Input::KBBackspace == ikey) {
					this->_text.erase(this->_text.end()-1);
				}
			}

			/**
			 * Função chamada toda vez que um caractere é digitado.
			 *
			 * @param ichar caractere digitado.
			 */
			virtual void onChar(unsigned int ichar) {
				this->_text += ichar;
			}

		public:
			///Inicializa a entrada de texto.
			InputText() {}

			///Finaliza a entrada de texto.
			~InputText() {}

			/**
			 * Obtém o texto na dodificação utf8.
			 *
			 * @return O texto na dodificação utf8.
			 */
			std::string text();

			/**
			 * Modifica o texto digitado, substituindo ele pelo que foi passado por parametro.
			 *
			 * @param itext Texto a ser colocado no lugar do atual.
			 */
			void text(std::string itext);

			///Limpa o texto digitado.
			void clear();

			///Ativa o recebimento de mensagens de entrada.
			void activate();

			///Desativa o recebimento de mensagens de entrada.
			void deactivate();
		};
	}  // namespace input
}  // namespace bolt

#endif /* BOLT_INPUT_INPUTTEXT_H_ */
