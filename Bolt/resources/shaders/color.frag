varying vec3 normal;
varying vec4 color;

layout (location = 0) out vec4 color_out;
layout (location = 1) out vec4 normal_out;

uniform sampler2D tex;

void main() {
	normal_out = vec4(normal.x, normal.y, normal.z, 1.0);
	color_out = color;
}
