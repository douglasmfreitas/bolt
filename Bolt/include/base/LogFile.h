/*
 * LogFile.h
 *
 *  Created on: 19/12/2012
 *      Author: Douglas M. Freitas
 */

#ifndef BOLT_BASE_LOGFILE_H_
#define BOLT_BASE_LOGFILE_H_

#include <base/Base.h>

#include <string>
#include <ctime>

namespace bolt {
	namespace base {

		/**
		 * Classe responsavel por registrar os eventos do log no arquivo de texto. Ela é utilizada pela classe Log.
		 *
		 * @author Douglas M. Freitas
		 */
		class LogFile {
		private:
			///Nome do arquivo de log.
			std::string _filename;

		public:
			/**
			 * Inicializa o arquivo de log com o cabeçalho contendo o horario de inicio passado por parametro
			 * e o título.
			 *
			 * @param ifilename Nome do arquivo de log.
			 * @param ititle Título do arquivo de log.
			 * @param itimeinfo Horario da criação do arquivo.
			 */
			LogFile(std::string ifilename, std::string ititle, struct tm *itimeinfo);

			/**
			 * Escreve o rodapé do arquivo de log.
			 */
			~LogFile();

			/**
			 * Registra um evento com o tipo, horario e mensagem passados por parametro.
			 *
			 * @param itype Tipo do evento.
			 * @param itimeinfo Horario em que o evento ocorreu.
			 * @param iline Mensagem a ser registrada no arquivo.
			 */
			void write(LogType itype, struct tm *itimeinfo, std::string iline);
		};

	}  // namespace base
}  // namespace bolt

#endif /* BOLT_BASE_LOGFILE_H_ */
