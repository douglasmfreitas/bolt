/*
 * Log.cpp
 *
 *  Created on: 19/12/2012
 *      Author: Douglas M. Freitas
 */

#include <base/Log.h>
#include <base/LogFile.h>
#include <base/Base.h>

#include <core/Window.h>

#include <cstdarg>
#include <cstdio>

#include <boost/thread.hpp>

#include <windows.h>

namespace bolt {
	namespace base {
		Log::Log(std::string ifilename, std::string ititle)
		:_main(NULL), _error(true), _warning(false), _info(false) {
			time_t rawtime;
			struct tm * timeinfo;
			time(&rawtime);
			timeinfo = localtime(&rawtime);

			this->_main = new LogFile(ifilename, ititle, timeinfo);
		}

		Log::~Log() {
			delete this->_main;
		}

		boost::mutex log_write_guard;
		void Log::write(LogType itype, struct tm *itimeinfo, std::string imsg) {
			boost::lock_guard<boost::mutex> lock(log_write_guard);
			this->_main->write(itype, itimeinfo, imsg);
		}

		void Log::show(LogType itype, std::string imsg) {
			switch(itype) {
				case Error:
					if(this->_error) {
						if(core::Window::get() && core::Window::get()->fullscreen()) {
							core::Window::get()->setActive(false);
							core::Window::get()->update();
						}
						MessageBox(NULL, sysEncode(imsg).c_str(), sysEncode("Erro").c_str(), MB_OK | MB_ICONERROR | MB_SYSTEMMODAL | MB_TOPMOST);
					}
					break;
				case Warning:
					if(this->_warning) {
						if(core::Window::get() && core::Window::get()->fullscreen()) {
							core::Window::get()->setActive(false);
							core::Window::get()->update();
						}
						MessageBox(NULL, sysEncode(imsg).c_str(), sysEncode("Aviso").c_str(), MB_OK | MB_ICONWARNING | MB_SYSTEMMODAL | MB_TOPMOST);
					}
					break;
				case Info:
					if(this->_info) {
						if(core::Window::get() && core::Window::get()->fullscreen()) {
							core::Window::get()->setActive(false);
							core::Window::get()->update();
						}
						MessageBox(NULL, sysEncode(imsg).c_str(), sysEncode("Informação").c_str(), MB_OK | MB_ICONINFORMATION | MB_SYSTEMMODAL | MB_TOPMOST);
					}
					break;
				default:
					break;
			}
		}
	}  // namespace base
}  // namespace bolt
