/*
 * Log.h
 *
 *  Created on: 19/12/2012
 *      Author: Douglas M. Freitas
 */

#ifndef BOLT_BASE_LOG_H_
#define BOLT_BASE_LOG_H_

#include <base/Base.h>

namespace bolt {
	namespace base {
		/**
		 * Classe responsavel pelos arquivos de log, ela deve ser inicializada antes de qualquer registro de evento.
		 * Antes da finalização do programa ela deve ser finalizada.
		 *
		 * O registro de eventos é feito pela função #log(), que é separada dessa classe.
		 */
		class Log {
		private:
			///Arquivo no qual as mensagens devem ser guardadas.
			LogFile *_main;

			///Indica se as mensagens de erro devem ser mostradas em uma caixa de mensagem.
			bool _error;
			///Indica se as mensagens de aviso devem ser mostradas em uma caixa de mensagem.
			bool _warning;
			///Indica se as mensagens de informação devem ser mostradas em uma caixa de mensagem.
			bool _info;

		public:
			/**
			 * Inicializa a instância atual os arquivos de log.
			 * @param ifilename Arquivo que deve conter os registros de eventos.
			 * @param ititle Título do registro de eventos.
			 */
			Log(std::string ifilename, std::string ititle);

			/**
			 * Finaliza a instância atual e os arquivos de log.
			 */
			~Log();

			/**
			 * Registra uma mensagem de log passada por parametro junto com o tipo e horario.
			 *
			 * @param itype Tipo da mensagem.
			 * @param itimeinfo Horario da ocorrência.
			 * @param imsg Mensagem a ser registrada.
			 */
			void write(LogType itype, struct tm *itimeinfo, std::string imsg);

			/**
			 * Exibe uma caixa de mensagem com a mensagem de log.
			 *
			 * @param itype Tipo da mensagem.
			 * @param imsg Mensagem a ser registrada.
			 */
			void show(LogType itype, std::string imsg);
		};
	}  // namespace base
}  // namespace bolt

#endif /* BOLT_BASE_LOG_H_ */
