/*
 * Window.cpp
 *
 *  Created on: 28/12/2012
 *      Author: Douglas M. Freitas
 */

#include <core/Window.h>

#include <core/Display.h>
#include <core/Win32.h>
#include <core/Input.h>
#include <core/Render.h>

#define BOLT_CORE_WINDOW_WNDCLASSEX "BoltWindow"

namespace bolt {
	namespace core {
		Window *Window::_instance = NULL;

		Window::Window(std::string ititle, size_t iwidth, size_t iheight, Display *idisplay, bool ifullscreen, bool imaximized, bool icenter)
		:_window_id(0), _fullscreen(ifullscreen), _active(false), _cursor_held(false), _display(idisplay), _input(NULL), _render(NULL), _fullscreen_hotkey(0) {
			///Atualiza a instância atual e evita que mais de uma seja criada.
			if(_instance) {
				throw base::FatalException(base::print("Singleton<Window> : Tentou criar um novo objeto de um singleton!"));
			}
			_instance = this;

			this->_window_id = (size_t)Win32::createWindow(ititle, 0, 0, iwidth, iheight, this->_display, icenter, this->_fullscreen);

			Win32::setProp(this->_window_id, "bolt::core::Window", (void*)this);

			WINDOWPLACEMENT *placement = new WINDOWPLACEMENT;
			placement->length = sizeof(*placement);
			Win32::setProp(this->_window_id, "bolt::core::Window.placement", (void*)placement);

			this->_input = new Input(this->_window_id);
			this->_render = new Render(this->_window_id);

			_fullscreen_hotkey = 0;
			if(!RegisterHotKey((HWND)this->_window_id, _fullscreen_hotkey
					, MOD_ALT | 0x4000
					, Win32::virtualkey(Input::KBEnter))) {
				throw CoreException(base::print("Window::Window : Não foi possivel registrar a hotkey %d.", _fullscreen_hotkey));
			}

			Win32::showWindow(this->_window_id, imaximized || this->_fullscreen);
			Win32::storeWindowPlacement(this->_window_id);
		}

		Window::~Window() {
			delete this->_input;

			this->_display->reset();
			Win32::showCursor(true);

			Win32::destroyWindow(this->_window_id);

			///Volta o ponteiro da instância atual para nulo após a atual ser deletada.
			_instance = NULL;
		}

		void Window::setActive(bool iactive) {
			if(this->_active != iactive) {
				this->_active = iactive;

				Win32::showCursor(!this->_active || !this->_cursor_held);
				Win32::clipCursor(this->_window_id, this->_cursor_held && this->_active);

				if(this->_fullscreen) {
					if(this->_active) {
						this->_display->active();
					} else {
						//voltar resolução natural e esconder janela
						ShowWindow((HWND)this->_window_id, SW_MINIMIZE);

						this->_display->reset();
					}
				}
			}
		}

		void Window::doHoldCursor() {
			if(this->_cursor_held && this->_active) {
				POINT p = Win32::screenCenter((HWND)this->_window_id);
				SetCursorPos(p.x, p.y);
			}
		}

		void Window::onResize(size_t iwidth, size_t iheight) {
			this->_render->onResize(iwidth, iheight);
		}

		bool Window::update() {
			this->_render->onUpdate();
			this->_input->onUpdate();

			this->doHoldCursor();

			MSG msg;
			while(this->_active?PeekMessage(&msg,NULL,0,0,PM_REMOVE):GetMessage(&msg,NULL,0,0)) {
				if (msg.message==WM_QUIT) {
					return false;
				} else {
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}
			}
			return true;
		}

		void Window::title(std::string ititle) {
			SetWindowText((HWND)this->_window_id, base::sysEncode(ititle).c_str());
		}

		void Window::fullscreen(bool ifullscreen) {
			if(this->_fullscreen != ifullscreen) {
				if(ifullscreen) {
					Win32::storeWindowPlacement(this->_window_id);
				}

				this->_fullscreen = ifullscreen;
				if(!this->_fullscreen) {
					this->_display->reset();
				} else {
					this->_display->active();
				}

				Win32::setBorders(this->_window_id, this->_fullscreen);

				if(!this->_fullscreen) {
					Win32::restoreWindowPlacement(this->_window_id);
				}
				Win32::showWindow(this->_window_id, this->_fullscreen);
			}
		}

		void Window::holdCursor(bool ihold) {
			if(this->_cursor_held != ihold) {
				this->_cursor_held = ihold;
				Win32::showCursor(!this->_cursor_held);
				Win32::clipCursor(this->_window_id, this->_cursor_held && this->_active);
			}
		}

		void Window::close() {
			PostQuitMessage(0);
		}
	}  // namespace core
}  // namespace bolt

