/*
 * main.cpp
 *
 *  Created on: 19/12/2012
 *      Author: Douglas M. Freitas
 */

#define WINVER 0x0502
#include <windows.h>

#include <iostream>

#include <base/Base.h>
#include <base/Log.h>
#include <core/Window.h>
#include <core/Display.h>
#include <core/Render.h>

#include <input/Input.h>
#include <input/InputChart.h>
#include <input/InputText.h>

#include <render/Texture.h>
#include <render/Framebuffer.h>
#include <render/Shader.h>

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <cstdio>

using namespace std;

#define WIDTH_BUFF 250
#define HEIGHT_BUFF 250

class TesteRL : public bolt::core::Render::Listener {
private:
	bool teste;
	float angle;
	float angle_buff;

	size_t width;
	size_t height;

	size_t frame;

	bolt::input::Input &input;
	bolt::render::Texture tex;
	bolt::render::Framebuffer buff;
	bolt::render::Shader stex;
	bolt::render::Shader scolor;

	virtual void onResize(size_t iwidth, size_t iheight) {
		width = iwidth;
		height = iheight;
	}

	virtual void onUpdate() {
		frame++;
		if(input["character"]["forth"][0].t()) {
			buff.resize(buff.width() + 25, buff.height() + 25);
		}
		if(input["character"]["back"][0].t()) {
			if(buff.width() > 25) {
				buff.resize(buff.width() - 25, buff.height() - 25);
			}
		}


		{
			buff.bind();

			glViewport(0, 0, buff.width(), buff.height());
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			gluPerspective(60.0f, float(buff.width())/float(buff.height()), 1.01f, 1000.0f);

			gluLookAt(0.0, 3.0, 1.5, 0, 0, 0.5f, 0, 0, 1);

			glClearColor(0.7, 0.7, 0.7, 1.0);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();

			if(input["character"]["act1"][0].p()) {
				angle_buff += 0.1f;
			}
			if(input["character"]["act2"][0].p()) {
				angle_buff -= 0.1f;
			}
			glRotatef(angle_buff, 0.0f, 0.0f, 1.0f);

			glEnable(GL_TEXTURE_2D);
			stex.bind();
			tex.bind();
			glBegin(GL_QUADS);
				glColor3f(1.0f, 1.0f, 1.0f);

				glTexCoord2f(0.0f, 0.0f);
				glNormal3f(0.0f, 1.0f, 0.0f);
				glVertex3f(0.5f, 0.0f, 0.0f);

				glTexCoord2f(1.0f, 0.0f);
				glNormal3f(0.0f, 1.0f, 0.0f);
				glVertex3f(-0.5f, 0.0f, 0.0f);

				glTexCoord2f(1.0f, 1.0f);
				glNormal3f(0.0f, 1.0f, 0.0f);
				glVertex3f(-0.5f, 0.0f, 1.0f);

				glTexCoord2f(0.0f, 1.0f);
				glNormal3f(0.0f, 1.0f, 0.0f);
				glVertex3f(0.5f, 0.0f, 1.0f);
			glEnd();
			stex.unbind();
			scolor.bind();
			glBegin(GL_QUADS);
				glColor3f(1.0f, 1.0f, 1.0f);

				glTexCoord2f(0.0f, 0.0f);
				glNormal3f(0.0f, 0.0f, 1.0f);
				glVertex3f(10.0f, 10.0f, 0.0f);

				glTexCoord2f(1.0f, 0.0f);
				glNormal3f(0.0f, 0.0f, 1.0f);
				glVertex3f(-10.0f, 10.0f, 0.0f);

				glTexCoord2f(1.0f, 1.0f);
				glNormal3f(0.0f, 0.0f, 1.0f);
				glVertex3f(-10.0f, -10.0f, 0.0f);

				glTexCoord2f(0.0f, 1.0f);
				glNormal3f(0.0f, 0.0f, 1.0f);
				glVertex3f(10.f, -10.0f, 0.0f);
			glEnd();
			glDisable(GL_TEXTURE_2D);

			glPushMatrix();
				glTranslatef(0.0f, 0.0f, 1.25f);
				static GLUquadric *obj = gluNewQuadric();
				glColor3f(0.9f, 0.2f, 0.2f);
				gluSphere(obj, 0.25, 12, 12);
			glPopMatrix();
			scolor.unbind();

			buff.unbind();
		}

		glViewport(0, 0, width, height);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(60.0f, float(width)/float(height), 0.01f, 1000.0f);

		//gluLookAt(0.9, 0.9, 0.5, -3, -2, 0, 0, 0, 1);
		gluLookAt(0.0, 2.0, 0.5, 0, 0, 0.5f, 0, 0, 1);

		glClearColor(0.2, 0.1, 0.4, 1.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		if(input["character"]["left"][0].p()) {
			angle += 0.1f;
		}
		if(input["character"]["right"][0].p()) {
			angle -= 0.1f;
		}
		glRotatef(angle, 0.0f, 0.0f, 1.0f);

		glEnable(GL_TEXTURE_2D);
		buff.color().bind();
		glBegin(GL_QUADS);
			glColor3f(1.0f, 1.0f, 1.0f);

			glTexCoord2f(0.0f, 0.0f);
			glNormal3f(0.0f, 1.0f, 0.0f);
			glVertex3f(0.5f, 0.0f, 0.0f);

			glTexCoord2f(1.0f, 0.0f);
			glNormal3f(0.0f, 1.0f, 0.0f);
			glVertex3f(-0.5f, 0.0f, 0.0f);

			glTexCoord2f(1.0f, 1.0f);
			glNormal3f(0.0f, 1.0f, 0.0f);
			glVertex3f(-0.5f, 0.0f, 1.0f);

			glTexCoord2f(0.0f, 1.0f);
			glNormal3f(0.0f, 1.0f, 0.0f);
			glVertex3f(0.5f, 0.0f, 1.0f);
		glEnd();
		buff.depth().bind();
		glBegin(GL_QUADS);
			glColor3f(1.0f, 1.0f, 1.0f);

			glTexCoord2f(0.0f, 0.0f);
			glNormal3f(0.0f, 1.0f, 0.0f);
			glVertex3f(1.5f, 0.0f, 0.0f);

			glTexCoord2f(1.0f, 0.0f);
			glNormal3f(0.0f, 1.0f, 0.0f);
			glVertex3f(0.5f, 0.0f, 0.0f);

			glTexCoord2f(1.0f, 1.0f);
			glNormal3f(0.0f, 1.0f, 0.0f);
			glVertex3f(0.5f, 0.0f, 1.0f);

			glTexCoord2f(0.0f, 1.0f);
			glNormal3f(0.0f, 1.0f, 0.0f);
			glVertex3f(1.5f, 0.0f, 1.0f);
		glEnd();
		buff.color1().bind();
		glBegin(GL_QUADS);
			glColor3f(1.0f, 1.0f, 1.0f);

			glTexCoord2f(0.0f, 0.0f);
			glNormal3f(0.0f, 1.0f, 0.0f);
			glVertex3f(-0.5f, 0.0f, 0.0f);

			glTexCoord2f(1.0f, 0.0f);
			glNormal3f(0.0f, 1.0f, 0.0f);
			glVertex3f(-1.5f, 0.0f, 0.0f);

			glTexCoord2f(1.0f, 1.0f);
			glNormal3f(0.0f, 1.0f, 0.0f);
			glVertex3f(-1.5f, 0.0f, 1.0f);

			glTexCoord2f(0.0f, 1.0f);
			glNormal3f(0.0f, 1.0f, 0.0f);
			glVertex3f(-0.5f, 0.0f, 1.0f);
		glEnd();
		glDisable(GL_TEXTURE_2D);

		bolt::core::Render::grid(10);

		glClear(GL_DEPTH_BUFFER_BIT);
		bolt::core::Render::origin(0.25f);
	}

public:
	TesteRL(bolt::input::Input &iinput)
	:teste(false), angle(0.0f), angle_buff(0.0f), width(0), height(0), frame(0), input(iinput)
	, tex("resources/diamond.png"), buff(WIDTH_BUFF,HEIGHT_BUFF, bolt::render::Framebuffer::NCB2)
	, stex("resources/shaders/teste.vert","resources/shaders/teste.frag")
	, scolor("resources/shaders/color.vert","resources/shaders/color.frag") {
		bolt::base::log(bolt::base::Info, "Imagen carregada: %dx%d", tex.width(), tex.height());
	}
	~TesteRL() {}
};


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR pCmdLine, int nCmdShow) {
	cout << "CMD: " << pCmdLine << endl;

	try {
		bolt::core::Display *display = new bolt::core::Display(bolt::core::Display::DEFAULT);
		bolt::core::Window *win = new bolt::core::Window("Bolt", 854, 480, display, false, false, true);

		bolt::input::Input input;

		TesteRL t(input);
		win->render()->bind(&t);


		while(win->update()) {
			if(input["menu"]["back"][0].t()) {
				win->close();
			}
			if(input["character"]["run"][0].t()) {
				bolt::base::log(bolt::base::Error, "Teste!");
			}
		}

		delete win;
		delete display;

	} catch (bolt::base::Exception &e) {
		bolt::base::log(bolt::base::Error, "Erro fatal: %s", e.what().c_str());
	}

	//delete log;

	return 0;
}
