/*
 * Framebuffer.h
 *
 *  Created on: 21/02/2013
 *      Author: Douglas M. Freitas
 */

#ifndef BOLT_RENDER_FRAMEBUFFER_H_
#define BOLT_RENDER_FRAMEBUFFER_H_

#include <render/Render.h>

#include <render/Texture.h>

namespace bolt {
	namespace render {
		class Framebuffer {
		public:
			typedef enum NColorBuffers {
				NCB1,
				NCB2
			} NColorBuffers;

		private:
			size_t _id;

			size_t _width;
			size_t _height;

			NColorBuffers _n_color_buffers;

			Texture _color_buffer_0;
			Texture _color_buffer_1;
			Texture _depth_buffer;

			void destroy();
			void create();

		public:
			Framebuffer(size_t iwidth, size_t iheight, NColorBuffers in_color_buffers = NCB1);
			~Framebuffer();

			void bind();
			void unbind();

			void resize(size_t iwidth, size_t iheight);

			const Texture &color() const {return _color_buffer_0;}
			const Texture &color1() const {return _color_buffer_1;}
			const Texture &depth() const {return _depth_buffer;}

			size_t width() const {return _width;}
			size_t height() const {return _height;}
		};
	}  // namespace render
}  // namespace bolt


#endif /* BOLT_RENDER_FRAMEBUFFER_H_ */
