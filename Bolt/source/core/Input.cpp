/*
 * Input.cpp
 *
 *  Created on: 12/01/2013
 *      Author: Douglas
 */

#include <core/Input.h>

#include <core/Win32.h>

namespace bolt {
	namespace core {
		Input *Input::_instance = NULL;

		Input::Input(size_t iwindow_id)
		:_window_id(iwindow_id), _listeners(), _last(Input::Null) {
			///Atualiza a instância atual e evita que mais de uma seja criada.
			if(_instance) {
				throw base::FatalException(base::print("Singleton<Input> : Tentou criar um novo objeto de um singleton!"));
			}
			_instance = this;
		}
		Input::~Input() {
			///Volta o ponteiro da instância atual para nulo após a atual ser deletada.
			_instance = NULL;
		}

		void Input::onKeyDown(KeyCode ikey) {
			for(size_t i=0 ; i<this->_listeners.size() ; i++) {
				this->_listeners[i]->onKeyDown(ikey);
			}
			this->_last = ikey;
		}

		void Input::onKeyUp(KeyCode ikey) {
			for(size_t i=0 ; i<this->_listeners.size() ; i++) {
				this->_listeners[i]->onKeyUp(ikey);
			}
		}

		void Input::onKeyMove(KeyCode iaxis, float ipos) {
			for(size_t i=0 ; i<this->_listeners.size() ; i++) {
				this->_listeners[i]->onKeyMove(iaxis, ipos);
			}
		}

		void Input::onChar(unsigned int ichar) {
			for(size_t i=0 ; i<this->_listeners.size() ; i++) {
				this->_listeners[i]->onChar(ichar);
			}
		}

		void Input::onUpdate() {
			this->onKeyMove(Input::MouseHu, 0.0f);
			this->onKeyMove(Input::MouseHd, 0.0f);
			this->onKeyMove(Input::MouseVu, 0.0f);
			this->onKeyMove(Input::MouseVd, 0.0f);
			this->onKeyMove(Input::MouseMu, 0.0f);
			this->onKeyMove(Input::MouseMd, 0.0f);

			this->_last = Input::Null;

			for(size_t i=0 ; i<this->_listeners.size() ; i++) {
				this->_listeners[i]->onUpdate();
			}
		}

		std::string Input::keyname(Input::KeyCode ikey) {
			switch(ikey) {
				case Null:
					return "[Null]";

				//Keyboard
				case KBEsc:
					return "Escape";

				case KBTab:
					return "Tab";
				case KBEnter:
					return "Enter";
				case KBSpace:
					return "Space";
				case KBBackspace:
					return "Backspace";

				case KBPageUp:
					return "Page Up";
				case KBPageDown:
					return "Page Down";
				case KBEnd:
					return "End";
				case KBHome:
					return "Home";
				case KBInsert:
					return "Insert";
				case KBDelete:
					return "Delete";
				case KBClear:
					return "Clear";

				case KBPrintScreen:
					return "Print Screen";
				case KBBreak:
					return "Pause/Break";
				case KBSleep:
					return "Sleep";
				case KBApplication:
					return "Application";

				case KBCapsLock:
					return "Caps Lock";
				case KBNumLock:
					return "Num Lock";
				case KBScrollLock:
					return "Scroll Lock";

				case KBLeft:
					return "Left";
				case KBUp:
					return "Up";
				case KBRight:
					return "Right";
				case KBDown:
					return "Down";

				case KB0:
					return "0";
				case KB1:
					return "1";
				case KB2:
					return "2";
				case KB3:
					return "3";
				case KB4:
					return "4";
				case KB5:
					return "5";
				case KB6:
					return "6";
				case KB7:
					return "7";
				case KB8:
					return "8";
				case KB9:
					return "9";

				case KBA:
					return "A";
				case KBB:
					return "B";
				case KBC:
					return "C";
				case KBD:
					return "D";
				case KBE:
					return "E";
				case KBF:
					return "F";
				case KBG:
					return "G";
				case KBH:
					return "H";
				case KBI:
					return "I";
				case KBJ:
					return "J";
				case KBK:
					return "K";
				case KBL:
					return "L";
				case KBM:
					return "M";
				case KBN:
					return "N";
				case KBO:
					return "O";
				case KBP:
					return "P";
				case KBQ:
					return "Q";
				case KBR:
					return "R";
				case KBS:
					return "S";
				case KBT:
					return "T";
				case KBU:
					return "U";
				case KBV:
					return "V";
				case KBW:
					return "W";
				case KBX:
					return "X";
				case KBY:
					return "Y";
				case KBZ:
					return "Z";

				case KBNum0:
					return "Num 0";
				case KBNum1:
					return "Num 1";
				case KBNum2:
					return "Num 2";
				case KBNum3:
					return "Num 3";
				case KBNum4:
					return "Num 4";
				case KBNum5:
					return "Num 5";
				case KBNum6:
					return "Num 6";
				case KBNum7:
					return "Num 7";
				case KBNum8:
					return "Num 8";
				case KBNum9:
					return "Num 9";
				case KBNumMult:
					return "Num *";
				case KBNumPlus:
					return "Num +";
				case KBNumMinus:
					return "Num -";
				case KBNumDel:
					return "Num Delete";
				case KBNumDiv:
					return "Num /";
				case KBNumPeriod:
					return "Num .";

				case KBF1:
					return "F1";
				case KBF2:
					return "F2";
				case KBF3:
					return "F3";
				case KBF4:
					return "F4";
				case KBF5:
					return "F5";
				case KBF6:
					return "F6";
				case KBF7:
					return "F7";
				case KBF8:
					return "F8";
				case KBF9:
					return "F9";
				case KBF10:
					return "F10";
				case KBF11:
					return "F11";
				case KBF12:
					return "F12";

				case KBLeftShift:
					return "Left Shift";
				case KBRightShift:
					return "Right Shift";
				case KBLeftCtrl:
					return "Left Ctrl";
				case KBRightCtrl:
					return "Right Ctrl";
				case KBLeftAlt:
					return "Left Alt";
				case KBRightAlt:
					return "Right Alt";
				case KBLeftSuper:
					return "Left Super";
				case KBRightSuper:
					return "Right Super";

				case KBCedille:
					return "Ç";
				case KBEqual:
					return "=";
				case KBComma:
					return ",";
				case KBMinus:
					return "-";
				case KBPeriod:
					return ".";
				case KBSemicolon:
					return ";";
				case KBApostrophe:
					return "";
				case KBAgudo:
					return "´";
				case KBBracketClose:
					return "]";
				case KBBracketOpen:
					return "[";
				case KBTil:
					return "~";
				case KBSlash:
					return "/";
				case KBBackslash:
					return "\\";

				//Mouse
				case MouseHu:
					return "Mouse H+";
				case MouseHd:
					return "Mouse H-";
				case MouseVu:
					return "Mouse V+";
				case MouseVd:
					return "Mouse V-";
				case MouseMu:
					return "Mouse Scroll+";
				case MouseMd:
					return "Mouse Scroll-";

				case MouseLeft:
					return "Mouse Left";
				case MouseRight:
					return "Mouse Right";
				case MouseMiddle:
					return "Mouse Middle";
				case MouseX1:
					return "Mouse X1";
				case MouseX2:
					return "Mouse X2";
				case MouseX3:
					return "Mouse X3";
				case MouseX4:
					return "Mouse X4";
			}
			return "[Unknown]";
		}

		void Input::bind(Input::Listener *ilistener) {
			std::vector<Input::Listener *>::iterator it;
			for(it=this->_listeners.begin() ; it!=this->_listeners.end() ; it++) {
				if((*it) == ilistener) {
					return;
				}
			}
			this->_listeners.push_back(ilistener);
		}

		void Input::unbind(Input::Listener *ilistener) {
			std::vector<Input::Listener *>::iterator it;
			for(it=this->_listeners.begin() ; it!=this->_listeners.end() ; it++) {
				if((*it) == ilistener) {
					this->_listeners.erase(it);
					return;
				}
			}
		}
	}  // namespace core
}  // namespace bolt
