/*
 * Input.h
 *
 *  Created on: 10/01/2013
 *      Author: Douglas
 */

#ifndef BOLT_CORE_INPUT_H_
#define BOLT_CORE_INPUT_H_

#include <core/Core.h>

#include <vector>
#include <map>
#include <set>

namespace bolt {
	namespace core {
		/**
		 * Classe resposavel por obter e simplificar a entrada do usuário. Além disso ela é responsavel por padronizar
		 * a entrada, eliminando as diferenças entre dois dispositivos diferentes, como o teclado e um joystick ou o
		 * mouse.
		 *
		 * A entrada é obtida atravez da classe #Listener, que está aninhada a essa classe.
		 *
		 * @author Douglas M. Freitas
		 */
		class Input {
		public:
			/**
			 * Códigos de teclas virtuais. Cada tecla do teclado possui uma representação nessa lista, assim como o
			 * mouse e qualquer outro dispositivo de entrada.
			 */
			typedef enum KeyCode {
				Null = 0,         //!< Tecla nula.

				//Teclado
				KBEsc = 0x10000,  //!< Escape;

				KBTab,            //!< Tab;
				KBEnter,          //!< Enter/Return;
				KBSpace,          //!< Space;
				KBBackspace,      //!< Backspace;

				KBPageUp,         //!< PageUp;
				KBPageDown,       //!< PageDown;
				KBEnd,            //!< End;
				KBHome,           //!< Home;
				KBInsert,         //!< Insert;
				KBDelete,         //!< Delete;
				KBClear,          //!< Clear;

				KBPrintScreen,    //!< PrintScreen/Sys Req;
				KBBreak,          //!< Pause/Break;
				KBSleep,          //!< Sleep;
				KBApplication,    //!< Application;

				KBCapsLock,       //!< Caps Lock;
				KBNumLock,        //!< Num Lock;
				KBScrollLock,     //!< Scroll Lock;

				KBLeft,           //!< Ceta para Esquerda;
				KBUp,             //!< Ceta para Cima;
				KBRight,          //!< Ceta para Direita;
				KBDown,           //!< Ceta para Baixo;

				KB0,              //!< Número 0;
				KB1,              //!< Número 1;
				KB2,              //!< Número 2;
				KB3,              //!< Número 3;
				KB4,              //!< Número 4;
				KB5,              //!< Número 5;
				KB6,              //!< Número 6;
				KB7,              //!< Número 7;
				KB8,              //!< Número 8;
				KB9,              //!< Número 9;

				KBA,              //!< Letra A;
				KBB,              //!< Letra B;
				KBC,              //!< Letra C;
				KBD,              //!< Letra D;
				KBE,              //!< Letra E;
				KBF,              //!< Letra F;
				KBG,              //!< Letra G;
				KBH,              //!< Letra H;
				KBI,              //!< Letra I;
				KBJ,              //!< Letra J;
				KBK,              //!< Letra K;
				KBL,              //!< Letra L;
				KBM,              //!< Letra M;
				KBN,              //!< Letra N;
				KBO,              //!< Letra O;
				KBP,              //!< Letra P;
				KBQ,              //!< Letra Q;
				KBR,              //!< Letra R;
				KBS,              //!< Letra S;
				KBT,              //!< Letra T;
				KBU,              //!< Letra U;
				KBV,              //!< Letra V;
				KBW,              //!< Letra W;
				KBX,              //!< Letra X;
				KBY,              //!< Letra Y;
				KBZ,              //!< Letra Z;

				KBNum0,           //!< Teclado numérico: 0;
				KBNum1,           //!< Teclado numérico: 1;
				KBNum2,           //!< Teclado numérico: 2;
				KBNum3,           //!< Teclado numérico: 3;
				KBNum4,           //!< Teclado numérico: 4;
				KBNum5,           //!< Teclado numérico: 5;
				KBNum6,           //!< Teclado numérico: 6;
				KBNum7,           //!< Teclado numérico: 7;
				KBNum8,           //!< Teclado numérico: 8;
				KBNum9,           //!< Teclado numérico: 9;
				KBNumMult,        //!< Teclado numérico: */Vezes;
				KBNumPlus,        //!< Teclado numérico: +/Mais;
				KBNumMinus,       //!< Teclado numérico: -/Menos;
				KBNumDel,         //!< Teclado numérico: Del/Delete;
				KBNumDiv,         //!< Teclado numérico: / /Dividir;
				KBNumPeriod,      //!< Teclado numérico: ./Ponto;

				KBF1,             //!< F1;
				KBF2,             //!< F2;
				KBF3,             //!< F3;
				KBF4,             //!< F4;
				KBF5,             //!< F5;
				KBF6,             //!< F6;
				KBF7,             //!< F7;
				KBF8,             //!< F8;
				KBF9,             //!< F9;
				KBF10,            //!< F10;
				KBF11,            //!< F11;
				KBF12,            //!< F12;

				KBLeftShift,      //!< Left Shift;
				KBRightShift,     //!< Right Shift;
				KBLeftCtrl,       //!< Left Ctrl;
				KBRightCtrl,      //!< Right Ctrl;
				KBLeftAlt,        //!< Left Alt;
				KBRightAlt,       //!< Right Alt;
				KBLeftSuper,      //!< Left Super/Windows;
				KBRightSuper,     //!< Right Super/Windows;

				KBCedille,        //!< Ç/Cedilha;
				KBEqual,          //!< =/Igual;
				KBComma,          //!< ,/Virgula;
				KBMinus,          //!< -/Menos;
				KBPeriod ,        //!< ./Ponto;
				KBSemicolon,      //!< ;/Ponto e virgula;
				KBApostrophe,     //!< '/Aspas;
				KBAgudo,          //!< ´/Agudo;
				KBBracketClose,   //!< ]/Fecha colchetes;
				KBBracketOpen,    //!< [/Abre colchetes;
				KBTil,            //!< ~/Til;
				KBSlash,          //!< / /Barra;
				KBBackslash,      //!< \ /Barra invertida;

				//Mouse
				MouseHu = 0x20000,//!< Eixo positivo horizontal do mouse;
				MouseHd,          //!< Eixo negativo horizontal do mouse;
				MouseVu,          //!< Eixo positivo vertical do mouse;
				MouseVd,          //!< Eixo negativo vertical do mouse;
				MouseMu,          //!< Roda do mouse, movimentação positiva;
				MouseMd,          //!< Roda do mouse, movimentação negativa;

				MouseLeft,        //!< Botão esquerdo do mouse;
				MouseRight,       //!< Botão direito do mouse;
				MouseMiddle,      //!< Botão do meio do mouse;
				MouseX1,          //!< Primeiro botão extra do mouse;
				MouseX2,          //!< Segundo botão extra do mouse;
				MouseX3,          //!< Terceiro botão extra do mouse;
				MouseX4           //!< Quarto botão extra do mouse;

			} KeyCode;

			/**
			 * Classe base para as classes que desejam receber a entrada do usuário.
			 */
			class Listener {
			protected:
				/**Função chamada toda vez que uma tecla é pressionada.
				 * @param ikey Tecla pressionada.*/
				virtual void onKeyDown(KeyCode ikey) {}

				/**Função chamada toda vez que uma tecla é solta.
				 * @param ikey Tecla solta.*/
				virtual void onKeyUp(KeyCode ikey) {}

				/**Função chamada toda vez que uma tecla se move.
				 * @param iaxis Tecla que se moveu.
				 * @param ipos Posição da tecla.*/
				virtual void onKeyMove(KeyCode iaxis, float ipos) {}

				/**Função chamada toda vez que um caractere é digitado.
				 * @param ichar caractere digitado.*/
				virtual void onChar(unsigned int ichar) {}

				///Função chamda toda vez que a janela atualiza o seu conteúdo.
				virtual void onUpdate() {}

			public:
				///Inicializa o listener.
				Listener() {}

				///Desvincula o objeto da entrada antes do objeto ser excluido.
				virtual ~Listener() {if(core::Input::get()){core::Input::get()->unbind(this);}}

				friend class Input;
			};

		private:
			///Identificador da janela a qual essa entrada está associada.
			size_t _window_id;

			///Lista de listeners associados a essa entrada.
			std::vector<Input::Listener *> _listeners;

			///Última tecla pressionada.
			KeyCode _last;

			/**
			 * Função chamada toda vez que uma tecla é pressionada.
			 * Além disso ela repassa o evento para todos os listeners.
			 *
			 * @param ikey Tecla pressionada.
			 */
			void onKeyDown(KeyCode ikey);

			/**
			 * Função chamada toda vez que uma tecla é solta.
			 * Além disso ela repassa o evento para todos os listeners.
			 *
			 * @param ikey Tecla solta.
			 */
			void onKeyUp(KeyCode ikey);

			/**
			 * Função chamada toda vez que uma tecla se move.
			 * Além disso ela repassa o evento para todos os listeners.
			 *
			 * @param iaxis Tecla que se moveu.
			 * @param ipos Posição da tecla.
			 */
			void onKeyMove(KeyCode iaxis, float ipos);

			/**
			 * Função chamada toda vez que um caractere é digitado.
			 * Além disso ela repassa o evento para todos os listeners.
			 *
			 * @param ichar caractere digitado.
			 */
			void onChar(unsigned int ichar);

			/**
			 * Função chamda toda vez que a janela atualiza o seu conteúdo.
			 * Além disso ela repassa o evento para todos os listeners.
			 */
			void onUpdate();

			///Ponteiro para a instância atual do singleton.
			static Input *_instance;

			///Impede que cópias do signleton sejam feitas.
			Input(const Input &);

		public:
			/**
			 * Inicializa os atributos.
			 * @param iwindow_id Identificador da janela a ser utilizada na criação das hotkeys.
			 */
			Input(size_t iwindow_id);

			///Desvincula todos os listeners antes da entrada ser deletada.
			~Input();

			/**
			 * Obtém o nome da tecla passada por parametro.
			 * @param ikey Tecla que se deseja obter o nome.
			 * @return Nome da tecla.
			 */
			static std::string keyname(Input::KeyCode ikey);

			/**
			 * Vincula o listener à entrada.
			 * @param ilistener Listener a ser vinculado.
			 */
			void bind(Input::Listener *ilistener);

			/**
			 * Desregistra o listener passado por parametro.
			 * @param ilistener #Listener a ser desregistrado.
			 */
			void unbind(Input::Listener *ilistener);

			/**
			 * Obtém a última tecla pressionada desde a última atualização.
			 *
			 * @return A última tecla pressionada.
			 */
			Input::KeyCode last() const {return this->_last;}

			///Este método obtém a instância atual do singleton.
			static Input *get() {
				return _instance;
			}

			friend class Listener;
			friend class Win32;
			friend class Window;
		};
	}  // namespace core
}  // namespace bolt

#endif /* BOLT_CORE_INPUT_H_ */
