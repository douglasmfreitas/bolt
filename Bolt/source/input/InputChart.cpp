/*
 * InputChart.cpp
 *
 *  Created on: 20/01/2013
 *      Author: Douglas
 */

#include <input/InputChart.h>

namespace bolt {
	namespace input {
		InputChart::InputChart(std::string iname, std::string idesc)
		:_name(iname), _desc(idesc) {}

		InputChart::~InputChart() {}

		void InputChart::bind(core::Input::KeyCode ikey, std::string icmd, size_t iplayer) {
			CmdMap::iterator it = this->_commands.find(icmd);
			if(it == this->_commands.end()) {
				throw InputException(base::print("InputChart::addCommand() : Tentou vincular uma tecla a um comando que não existe: \"%s\".", icmd.c_str()));
			}
			unbind(ikey);
			this->_bindings.insert(std::pair<core::Input::KeyCode, Command*>(ikey, &it->second[iplayer]));
		}

		void InputChart::unbind(core::Input::KeyCode ikey) {
			std::map<core::Input::KeyCode, Command*>::iterator it = this->_bindings.find(ikey);
			if(it != this->_bindings.end()) {
				this->_bindings.erase(it);
			}
		}

		void InputChart::addCommand(std::string iid, std::string iname, std::string idesc) {
			if(this->_commands.find(iid) != this->_commands.end()) {
				throw InputException(base::print("InputChart::addCommand() : Tentou criar um comando com um identificador já existente: \"%s\".", iid.c_str()));
			}

			this->_commands.insert(CmdMap::value_type(iid, CommandRow(iname, idesc)));
		}

		void InputChart::activate() {
			core::Input::get()->bind(this);
		}

		void InputChart::deactivate() {
			core::Input::get()->unbind(this);
		}
	}  // namespace input
}  // namespace bolt
