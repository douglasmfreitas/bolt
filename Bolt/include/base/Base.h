/*
 * Base.h
 *
 *  Created on: 13/12/2012
 *      Author: Douglas M. Freitas
 */

#ifndef BOLT_BASE_BASE_H_
#define BOLT_BASE_BASE_H_

#include <base/Exception.h>

#include <cstdlib>
#include <string>

namespace bolt {
	/**
	 * Este namespace contém funções e classes que são utilizadas por todos os componentes do sistema.
	 * Como por exemplo a função #print() ou a classe Exception.
	 *
	 * @author Douglas M. Freitas
	 */
	namespace base {

		class Log;
		class LogFile;

		/**
		 * Tipos de mensagens de erro. São usados ao criar uma nova mensagem com a função #log().
		 */
		typedef enum LogType {
			Error,	//!< Mensagem de erro.
			Warning,//!< Mensagem de aviso.
			Info,	//!< Mensagem de informação.
			Nothing	//!< Mensagem insignificante.
		} Type;

		/**
		 * Essa função funciona de forma semelhante à printf da stdio.h, só que a saida ela retorna como uma string.
		 * Ela foi criada para simplificar a formatação de texto com a utilização de variaveis de tipos diferentes
		 * na sua montagem.
		 *
		 * @param iformat String com a formatação, semelhante ao formato do printf.
		 * @param ... Parametros a serem usados para montar a string final.
		 * @return Uma string com o resultado da formatação.
		 */
		std::string print(const char *iformat, ...);

		/**
		 * Converte o texto recebido por parametro de UTF8 para a codificação aceita pelo sistema.
		 * no caso do windows é ISO-8859-1.
		 * @param itext Texto a ser traduzido.
		 * @return Texto traduzido.
		 */
		std::string sysEncode(std::string itext);

		/**
		 * Converte o texto recebido por parametro de UTF8 para a codificação passada por parametro.
		 * @param itext Texto a ser traduzido.
		 * @param iencoding Codificação alvo.
		 * @return Texto traduzido.
		 */
		std::string encode(std::string itext, std::string iencoding);

		/**
		 * Esta função é responsavel por registrar os eventos utilizando a instância atual da classe Log.
		 * Ela funciona da mesma forma que a função printf da stdio.h.
		 * @param itype Tipo da mensagem.
		 * @param iformat Formato da mesma forma do utilizado na função printf.
		 * @param ... Parametros a serem usados na mensagem, assim como usados na função printf.
		 */
		void log(LogType itype, std::string iformat, ...);

		/**
		 * Exceção lançada pelo sistema de log.
		 */
		class LogException : public Exception {
		public:
			LogException(std::string iwhat) : Exception(iwhat) {}
			virtual std::string what() const {return "LogException : " + this->_what;}
		};
	}  // namespace base
}  // namespace bolt

#endif /* BOLT_BASE_BASE_H_ */
