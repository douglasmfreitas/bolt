/*
 * Base.cpp
 *
 *  Created on: 16/12/2012
 *      Author: Douglas M. Freitas
 */

#include <base/Base.h>
#include <base/Log.h>

#include <cstdarg>
#include <cstdio>

#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <boost/locale.hpp>

#include <windows.h>

#define BOLT_BASE_PRINTF_BUFFER_CHUNK_SIZE 1024

namespace bolt {
	namespace base {

		std::string print(const char *iformat, ...) {
			for(size_t n = BOLT_BASE_PRINTF_BUFFER_CHUNK_SIZE ;; n+=BOLT_BASE_PRINTF_BUFFER_CHUNK_SIZE) {
				char buffer[n];

				va_list args;
				va_start(args, iformat);
				size_t nf = vsnprintf(buffer, n, iformat, args);
				va_end(args);

				if(nf < n) {
					return std::string(buffer);
				}
			}
			return std::string();
		}

		std::string sysEncode(std::string itext) {
			return boost::locale::conv::from_utf(itext,"ISO-8859-1");
		}

		std::string encode(std::string itext, std::string iencoding) {
			return boost::locale::conv::from_utf(itext, iencoding);
		}

		void log(LogType itype, std::string iformat, ...) {
			static Log log("log.txt", "TesteBolt");

			time_t rawtime;
			struct tm * timeinfo;
			time(&rawtime);
			timeinfo = localtime(&rawtime);

			std::string msg;

			for(size_t n = BOLT_BASE_PRINTF_BUFFER_CHUNK_SIZE ; msg.empty() ; n+=BOLT_BASE_PRINTF_BUFFER_CHUNK_SIZE) {
				char buffer[n];

				va_list args;
				va_start(args, iformat);
				size_t nf = vsnprintf(buffer, n, iformat.c_str(), args);
				va_end(args);

				if(nf < n) {
					msg = buffer;
				}
			}

			log.write(itype, timeinfo, msg);
			log.show(itype, msg);
		}

	}  // namespace base
}  // namespace bolt

