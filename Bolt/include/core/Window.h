/*
 * Window.h
 *
 *  Created on: 28/12/2012
 *      Author: Douglas M. Freitas
 */

#ifndef BOLT_CORE_WINDOW_H_
#define BOLT_CORE_WINDOW_H_

#include <core/Core.h>

#include <vector>
#include <string>

namespace bolt {
	namespace core {
		/**
		 * Classe responsavel pela janela do programa além do controle da atualização dos outros componentes do
		 * namespace. Com exceção do controle da resolução do modo de tela cheia, todas as terefas relacionadas ao
		 * controle da janela são realizadas por essa classe.
		 */
		class Window {
		private:
			///Identificador da janela utilizado pelo sistema operacional. No caso do Windows é o HWND.
			size_t _window_id;

			///Indica se o modo tela cheia está ativado ou não.
			bool _fullscreen;

			///Indica se a janela está ativa ou não.
			bool _active;

			///Indica se o cursor está preso na janela ou não.
			bool _cursor_held;

			///Instância do display a ser utilizado pelo objeto.
			Display *_display;

			///Instância da entrada associada a esse objeto.
			Input *_input;

			///Instância do render associado à janela.
			Render *_render;

			///Identificador da hotkey que ativa a fullscreen.
			int _fullscreen_hotkey;

			/**
			 * Método chamado toda vez que o estado de ativo/inativo é alterado.
			 *
			 * @param iactive indica se a janela está sendo ativada ou desativada.
			 */
			void setActive(bool iactive);

			///Realiza a ação de segurar o cursor dentro da janela.
			void doHoldCursor();

			/**
			 * Este método é chamado toda vez que a janela sofre alguma alteração de tamanho.
			 *
			 * @param iwidth Nova largura da janela.
			 * @param iheight Nova altura da janela.
			 */
			void onResize(size_t iwidth, size_t iheight);

			///Ponteiro para a instância atual do singleton.
			static Window *_instance;

			///Impede que cópias do signleton sejam feitas.
			Window(const Window &);

		public:
			/**
			 * Cria a janela e exibe ela com as configurações passadas por parametro.
			 *
			 * @param ititle Título da janela.
			 * @param iwidth Largura da janela a ser utilizada no modo janela.
			 * @param iheight Altura da janela a ser utilizada no modo janela.
			 * @param idisplay Instância da classe Display a ser utilizada.
			 * @param ifullscreen Indica se a janela deve ser criada já no modo tela cheia.
			 * @param imaximized Indica se a janela deve ser criada maximizada.
			 * @param icenter Indica se a janela deve ser criada no centro da tela.
			 */
			Window(std::string ititle, size_t iwidth, size_t iheight, Display *idisplay, bool ifullscreen=false, bool imaximized=false, bool icenter=true);

			/**
			 * Destroi a janela junto com a instância da entrada e do render. A instância do display é deixada
			 */
			~Window();

			/**
			 * Obtém o identificador da janela. O que é utilizado pelo sistema operacional.
			 *
			 * @return O identificador da janela.
			 */
			size_t id() const {return (size_t)this->_window_id;}

			/**
			 * Atualiza a janela, processando as mensagens que estão na fila. Esta função bloqueia enquanto a janela
			 * estiver inativa.
			 *
			 * @return Retorna false quando a janela é fechada e true quando ela continua aberta.
			 */
			bool update();

			/**
			 * Altera o título da janela.
			 *
			 * @param ititle Novo título da janela.
			 */
			void title(std::string ititle);

			/**
			 * Alterna entre o modo janela e o modo tela cheia.
			 *
			 * @param ifullscreen
			 */
			void fullscreen(bool ifullscreen);

			/**
			 * Verifica se a janela está no modo fullscreen ou não.
			 * @return Retorna true quando a janela está no modo fullscreen e false caso contrario.
			 */
			bool fullscreen() const {return this->_fullscreen;}

			/**
			 * Ordena que a janela prenda o cursor no centro da janela ou, caso ele esteja preso, solte o mesmo.
			 *
			 * @param ihold Deve ser true para que o cursor seja preso e false para que seja solto.
			 */
			void holdCursor(bool ihold);

			/**
			 * Verifica se o cursor está preso na janela.
			 * @return Retorna true quando o cursor está preso e false caso contrario.
			 */
			bool cursorHeld() const {return this->_cursor_held;}

			/**
			 * Fecha a janela.
			 */
			void close();

			/**
			 * Obtém o display associado à janela.
			 *
			 * @return Instância da classe Display.
			 */
			Display *display() {return this->_display;}

			/**
			 * Obtém a entrada associada à janela.
			 *
			 * @return Instância da classe Input.
			 */
			Input *input() {return this->_input;}

			/**
			 * Obtém o render associado à janela.
			 *
			 * @return Instância da classe Render.
			 */
			Render *render() {return this->_render;}

			///Este método obtém a instância atual do singleton.
			static Window *get() {
				return _instance;
			}

			friend class Win32;
			friend class base::Log;
		};

	}  // namespace core
}  // namespace bolt


#endif /* BOLT_CORE_WINDOW_H_ */
