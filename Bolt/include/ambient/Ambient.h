/*
 * Ambient.h
 *
 *  Created on: 26/02/2013
 *      Author: Douglas
 */

#ifndef AMBIENT_H_
#define AMBIENT_H_

namespace bolt {
	namespace ambient {

		class Mesh;

		class Camera;
		class Light;

		class Node;

		class Ambient;

	}  // namespace ambient
}  // namespace bolt


#endif /* AMBIENT_H_ */
