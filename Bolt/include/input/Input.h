/*
 * Input.h
 *
 *  Created on: 18/01/2013
 *      Author: Douglas
 */

#ifndef BOLT_INPUT_INPUT_H_
#define BOLT_INPUT_INPUT_H_

#include <base/Base.h>

#include <map>
#include <string>

namespace bolt {
	/**
	 * Este namespace contém o módulo responsavel por tratar a entrada e simplificar ela para que fique facil a sua
	 * utilização. A entrada é abstraida em comandos, cada comando é reunido em uma linha de quatro comandos, um para
	 * cada jogador, cada linha recebe um nome e uma descrição. As linhas são agrupadas em tabelas, cada tabela possui
	 * um nome e recebem a entrada do bolt::core::Input. Além disso as tabelas realizam a ligação entre a tecla e o comando.
	 * Por fim as tabelas são reunidas na classe Input.
	 *
	 * Para obter um comando 'c' da tabela 'x' de do jogador 0 de um objeto da classe Input chamado in, basta fazer:
	 * in[x][c][0]
	 * onde 'c' e 'x' são strings.
	 *
	 * Além disso a classe InputText cuida da entrada de texto.
	 *
	 * @author Douglas M. Freitas
	 */
	namespace input {
		class Command;
		class CommandRow;
		class InputChart;
		class InputText;
		class Input;

		/**
		 * Exceção a ser lançada pelos componentes desse namespace.
		 */
		class InputException : public base::Exception {
		public:
			InputException(std::string iwhat) : Exception(iwhat) {}
			virtual std::string what() const {return "InputException : " + this->_what;}
		};

		/**
		 * Esta classe é responsavel por agrupar e gerenciar os objetos responsaveis pela entrada, como as tabelas
		 * de entrada e a entrada de texto.
		 *
		 * As tabelas agrupam os comandos e cada uma recebe a entrada da classe bolt::core::Input. Já a entrada de texto
		 * recebe a entrada em modo texto. Por padrão as tabelas são inicializadas ativadas e a entrada de texto
		 * desativada.
		 *
		 * @author Douglas M. Freitas
		 */
		class Input {
		private:
			///Tipo do mapa de tabelas de entrada.
			typedef std::map<std::string, InputChart *> ChartMap;

			///Entrada de texto a ser utilizada.
			InputText *_text_input;

			///Mapa de tabelas de entrada.
			ChartMap _input_charts;

		public:
			///Inicializa o objeto.
			Input();

			///Finaliza o objeto.
			~Input();

			/**
			 * Obtém a referência da entrada de texto.
			 *
			 * @return Entrada de texto.
			 */
			InputText &text();

			/**
			 * Adiciona uma tabela de entrada.
			 *
			 * @param iid Identificador da tabela.
			 * @param iname Nome da tabela.
			 * @param idesc Descrição da tabela.
			 */
			void addInputChart(std::string iid, std::string iname, std::string idesc);

			/**
			 * Obtém a quantidade de tabelas de entrada.
			 *
			 * @return Quantidade de tabelas.
			 */
			size_t size() {
				return this->_input_charts.size();
			}

			/**
			 * Obtém uma tabela de entrada de acordo com a posição dela, onde a ordem é definida pelo identificador.
			 * Este método deve ser utilizado quando se deseja obter todas as tabelas.
			 *
			 * @param iid Posição da tabela de entrada.
			 * @return Tabela de entrada referênte a posição.
			 */
			InputChart &operator[](size_t iid) {
				if(iid >= this->_input_charts.size()) {
					throw InputException(base::print("Input::operator[] : Existe nenhuma tabela de entrada com o identificador \"%d\".", iid));
				}

				ChartMap::iterator it = this->_input_charts.begin();
				for(size_t i=0 ; i<iid ; i++, it++);
				return *(it->second);
			}

			/**
			 * Obtém a tabela de entrada vinculada ao identificador passado por parametro.
			 *
			 * @param iid Identificador da tabela.
			 * @return Tabela de entrada vinculada ao identificador.
			 */
			InputChart &operator[](std::string iid) {
				ChartMap::iterator it = this->_input_charts.find(iid);
				if(it != this->_input_charts.end()) {
					return *(it->second);
				} else {
					throw InputException(base::print("Input::operator[] : Existe nenhuma tabela de entrada com o identificador \"%s\".", iid.c_str()));
				}
			}
		};

	}  // namespace input
}  // namespace bolt


#endif /* BOLT_INPUT_INPUT_H_ */
