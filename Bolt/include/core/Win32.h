/*
 * Win32.h
 *
 *  Created on: 08/01/2013
 *      Author: Douglas
 */

#ifndef BOLT_CORE_WIN32_H_
#define BOLT_CORE_WIN32_H_

#include <core/Core.h>

#define WINVER 0x0502
#include <windows.h>
#include <windowsx.h>

#include <core/Input.h>

namespace bolt {
	namespace core {
		/**
		 * Esta classe reúne as implementações de funcionalidades específicas do Windows.
		 * Como a conversão das teclas virtuais da classe Input para as teclas virtuais do windows, a obtenção do
		 * centro da janela ou a implementação da função #WindowProc(). Essas função são separadas do resto do programa
		 * para que seja facil portar ele para outros sistemas operacionais.
		 *
		 * @author Douglas M. Freitas
		 */
		class Win32 {
		public:
			/**
			 * Converte uma tecla virtual do Windows em uma tecla virtual da classe Input.
			 *
			 * @param ivk Tecla virtual do Windows.
			 * @return Tecla virtual da classe Input.
			 */
			static Input::KeyCode keycode(size_t ivk);

			/**
			 * Converte uma tecla virtual da classe Input em uma tecla virtual do Windows.
			 *
			 * @param ikey Tecla virtual da classe Input.
			 * @return Tecla virtual do Windows.
			 */
			static size_t virtualkey(Input::KeyCode ikey);

			/**
			 * Obtém o centro da janela em relação à própria janela.
			 *
			 * @param iwindow_id Identificador da janela da qual se deseja obter o centro.
			 * @return centro da janela.
			 */
			static POINT clientCenter(HWND iwindow_id);

			/**
			 * Obtém o centro da janela em relação a tela do monitor.
			 *
			 * @param iwindow_id Identificador da janela da qual se deseja obter o centro.
			 * @return centro da janela.
			 */
			static POINT screenCenter(HWND iwindow_id);

			/**
			 * Função responsavel por criar uma janela de acordo com as configurações passadas por parametro.
			 *
			 * @param ititle Título da janela.
			 * @param ipos_x Posição x da janela.
			 * @param ipos_y Posição y da janela.
			 * @param iwidth Altura da janela.
			 * @param iheight Largura da janela.
			 * @param idisplay Instância da classe Display utilicado para calcular o centro da tela.
			 * @param icenter Indica se a janela deve ser posicionada no centro da tela.
			 * @param iborders Indica se a janela deve ter bordas.
			 * @return Retorna o identificador da janela.
			 */
			static size_t createWindow(std::string ititle, int ipos_x, int ipos_y, size_t iwidth, size_t iheight, Display *idisplay, bool icenter, bool iborders);

			/**
			 * Destroi a janela passada por parametro.
			 *
			 * @param iwindow_id Identificador da janela a ser destruida.
			 */
			static void destroyWindow(size_t iwindow_id);

			/**
			 * Inicializa o OpenGL para que seja usado na renderização do jogo.
			 *
			 * @param iwindow_id Identificador da janela a qual o OpenGL deverá ser vinculado.
			 * @param odevice_context Endereço da variavel onde o contexto do dispositivo deverá ser colocado.
			 * @param oopengl_context Endereço da variavel onde o contexto do OpenGL deverá ser colocado.
			 */
			static void createOpenGL(size_t iwindow_id, void **odevice_context, void **oopengl_context);

			/**
			 * Método responsavel por finalizar o OpenGL.
			 *
			 * @param iwindow_id Identificador da janela a qual o OpenGL está vinculado.
			 * @param idevice_context Contexto do dispositivo.
			 * @param iopengl_context Contexto do OpenGL.
			 */
			static void destroyOpenGL(size_t iwindow_id, void *idevice_context, void *iopengl_context);

			/**
			 * Exibe a janela na tela.
			 *
			 * @param iwindow_id Identificador da janela a ser exibida.
			 * @param imaximized Indica se a janela deve ser exibida maximizada.
			 */
			static void showWindow(size_t iwindow_id, bool imaximized);

			/**
			 * Retira ou adiciona bordas à janela.
			 *
			 * @param iwindow_id Identificador da janela cuja as bordas devem ser alteradas.
			 * @param iborders Indica se a janela deve ter bordas.
			 */
			static void setBorders(size_t iwindow_id, bool iborders);

			/**
			 * Exibe o cursor ou esconde o mesmo.
			 * @param ishow Use true para mostrar o cursor e false para esconder.
			 */
			static void showCursor(bool ishow);

			/**
			 * Previne que o cursor saia de dentro da janela.
			 *
			 * @param iwindow_id Identificador da janela.
			 * @param iclip Use true para ativar o efeito e false para desativar.
			 */
			static void clipCursor(size_t iwindow_id, bool iclip);

			/**
			 * Adiciona uma propriedade à janela.
			 *
			 * @param iwindow_id Identificador da janela.
			 * @param iname Nome da propriedade a ser adicionada.
			 * @param prop Ponteiro para a propriedade a ser adicionada.
			 */
			static void setProp(size_t iwindow_id, std::string iname, void *prop);

			/**
			 * Obtem uma propriedade da janela.
			 * @param iwindow_id Identificador da janela.
			 * @param iname Nome da propriedade.
			 * @return Ponteiro para a propriedade.
			 */
			static void *getProp(size_t iwindow_id, std::string iname);

			/**
			 * Guarda o posicionamento da janela.
			 * @param iwindow_id Identificador da janela.
			 */
			static void storeWindowPlacement(size_t iwindow_id);

			/**
			 * Restaura o posicionamento da janela guardado anteriormente.
			 * @param iwindow_id Identificador da janela.
			 */
			static void restoreWindowPlacement(size_t iwindow_id);

			/**
			 * Função de callback usada para o processamento das mensagens da janela.
			 */
			static LRESULT CALLBACK WindowProc(HWND iwindow_handler, UINT imessage, WPARAM iwparam, LPARAM ilparam);
		};
	}  // namespace core
}  // namespace bolt


#endif /* BOLT_CORE_WIN32_H_ */
