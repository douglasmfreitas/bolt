varying vec3 normal;
varying vec4 color;

void main() {
	gl_TexCoord[0] = gl_MultiTexCoord0;
	
	normal = normalize(gl_NormalMatrix * gl_Normal);
	color = gl_Color;
	
	gl_Position = ftransform();
}
