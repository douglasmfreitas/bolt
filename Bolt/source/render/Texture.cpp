/*
 * Texture.cpp
 *
 *  Created on: 20/02/2013
 *      Author: Douglas
 */

#include <render/Texture.h>

#include <FreeImage.h>

#include <GL/glew.h>
#include <GL/gl.h>

namespace bolt {
	namespace render {
		Texture::Texture(size_t iwidth, size_t iheight)
		:_id(0), _type(0), _width(0), _height(0) {
			loadBlank(iwidth, iheight);
		}

		Texture::Texture(const char *ifilename)
		:_id(0), _type(0), _width(0), _height(0) {
			loadImage(ifilename);
		}

		void Texture::loadBlank(size_t iwidth, size_t iheight) {
			unload();

			_type = GL_TEXTURE_2D;
			_width = iwidth;
			_height = iheight;

			GLubyte* texture_data = new GLubyte[4*_width*_height];

			for(size_t j= 0; j<_width*_height; j++){
				texture_data[j*4+0]= 255;
				texture_data[j*4+1]= 255;
				texture_data[j*4+2]= 255;
				texture_data[j*4+3]= 255;
			}

			glGenTextures(1, &_id);
			glBindTexture(GL_TEXTURE_2D, _id);
			glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA, _width, _height, 0, GL_RGBA,GL_UNSIGNED_BYTE,(GLvoid*)texture_data );
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

			delete [] texture_data;

			GLenum huboError = glGetError();
			if(huboError) {
				throw RenderException(base::print("Texture::loadBlank() : Não foi possivel carregar a textura '%dx%d'.", _width, _height));
			}
		}

		void Texture::loadImage(const char *ifilename) {
			unload();

			FREE_IMAGE_FORMAT formato = FreeImage_GetFileType(ifilename,0);
			FIBITMAP* image_source = FreeImage_Load(formato, ifilename);

			FIBITMAP* temp = image_source;
			image_source = FreeImage_ConvertTo32Bits(image_source);
			FreeImage_Unload(temp);

			_width = FreeImage_GetWidth(image_source);
			_height = FreeImage_GetHeight(image_source);
			_type = GL_TEXTURE_2D;

			glGenTextures(1, &_id);
			glBindTexture(GL_TEXTURE_2D, _id);
			glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA, _width, _height, 0, GL_BGRA,GL_UNSIGNED_BYTE,(GLvoid*)FreeImage_GetBits(image_source) );
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

			FreeImage_Unload(image_source);

			GLenum huboError = glGetError();
			if(huboError) {
				throw RenderException(base::print("Texture::loadImage() : Não foi possivel carregar a textura '%s'.", ifilename));
			}
		}

		void Texture::loadColorBuffer(size_t iwidth, size_t iheight) {
			unload();

			_width = iwidth;
			_height = iheight;

			_type = GL_TEXTURE_2D;

			glGenTextures(1, &_id);
			glBindTexture(GL_TEXTURE_2D, _id);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, _width, _height, 0, GL_RGBA,GL_UNSIGNED_BYTE, NULL);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

			GLenum huboError = glGetError();
			if(huboError) {
				throw RenderException(base::print("Texture::loadColorBuffer() : Não foi possivel carregar a textura '%dx%d'. Id do erro: %d", _width, _height, huboError));
			}
		}

		void Texture::loadDepthBuffer(size_t iwidth, size_t iheight) {
			unload();

			_width = iwidth;
			_height = iheight;

			_type = GL_TEXTURE_2D;

			glGenTextures(1, &_id);
			glBindTexture(GL_TEXTURE_2D, _id);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, _width, _height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

			GLenum huboError = glGetError();
			if(huboError) {
				throw RenderException(base::print("Texture::loadDepthBuffer() : Não foi possivel carregar a textura '%dx%d'.", _width, _height));
			}
		}

		void Texture::unload() {
			if(_id || _type || _width || _height) {
				glDeleteTextures(1, &_id);
			}
		}

		Texture::~Texture() {
			unload();
		}

		void Texture::bind() const {
			glBindTexture(_type, _id);
		}
	}  // namespace render
}  // namespace bolt
