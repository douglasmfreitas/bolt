/*
 * Display.cpp
 *
 *  Created on: 28/12/2012
 *      Author: Douglas M. Freitas
 */

#include <core/Display.h>

#define WINVER 0x0502
#include <windows.h>
#include <algorithm>

namespace bolt {
	namespace core {
		const size_t Display::DEFAULT = 0xFFFFFFFF;

		void Display::active() {
			Mode display_mode = this->mode(this->_selected);
			if(display_mode._width == 0 && display_mode._height == 0) {
				throw CoreException("Display::active() : Tentou utilizar uma modo de exibição não inicializado!");
			}

			this->_active = true;

			DEVMODE screen_settings;
			screen_settings.dmSize = sizeof(DEVMODE);
			EnumDisplaySettings(NULL,ENUM_CURRENT_SETTINGS,&screen_settings);
			if(screen_settings.dmPelsWidth != display_mode._width && screen_settings.dmPelsHeight != display_mode._height) {

				memset(&screen_settings,0,sizeof(screen_settings));
				screen_settings.dmSize=sizeof(screen_settings);
				screen_settings.dmPelsWidth	= display_mode._width;
				screen_settings.dmPelsHeight	= display_mode._height;
				screen_settings.dmBitsPerPel	= 32;
				screen_settings.dmFields=DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

				if (ChangeDisplaySettings(&screen_settings,CDS_FULLSCREEN)!=DISP_CHANGE_SUCCESSFUL) {
					throw CoreException(base::print("Display::active() : Não foi possivel mudar a resolução da tela %dx%d", display_mode._width, display_mode._height));
				} else {
					base::log(base::Info, "Alterando a resolução da tela para: %dx%d", display_mode._width, display_mode._height);
				}
			}
		}

		void Display::reset() {
			this->_active = false;

			ChangeDisplaySettings(NULL,0);
		}

		void Display::update() {
			{
				DEVMODE screen_settings;
				screen_settings.dmSize = sizeof(DEVMODE);
				EnumDisplaySettings(NULL,ENUM_REGISTRY_SETTINGS,&screen_settings);
				this->_default._width = screen_settings.dmPelsWidth;
				this->_default._height = screen_settings.dmPelsHeight;
			}
			int i=0;
			this->_display_list.clear();
			DEVMODE screen_settings;
			screen_settings.dmSize = sizeof(DEVMODE);
			while(EnumDisplaySettings(NULL,i,&screen_settings)) {
				if(screen_settings.dmBitsPerPel == 32) {
					Mode disp(screen_settings.dmPelsWidth, screen_settings.dmPelsHeight);
					unsigned int n;
					for(n=0 ; n<this->_display_list.size() && (this->_display_list[n]._width != disp._width || this->_display_list[n]._height != disp._height); n++);
					if(n == this->_display_list.size() && disp._width >= 640 && disp._height >= 480) {
						this->_display_list.push_back(disp);
					}
				}
				i++;
			}
			std::sort(this->_display_list.begin(), this->_display_list.end());
		}
	}
}
