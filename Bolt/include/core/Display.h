/*
 * Display.h
 *
 *  Created on: 28/12/2012
 *      Author: Douglas M. Freitas
 */

#ifndef BOLT_CORE_DISPLAY_H_
#define BOLT_CORE_DISPLAY_H_

#include <core/Core.h>

#include <vector>

namespace bolt {
	namespace core {
		/**
		 * Esta classe é responsavel pela resolução ativa do monitor, toda vez que o programa precisa entrar em modo
		 * tela cheia ele utiliza esta classe para ajustar a resolução. Também é esta classe que guarda o modo de tela
		 * cheia a ser utilizado.
		 *
		 * @author Douglas M. Freitas
		 */
		class Display {
		public:
			/**
			 * Classe responsavel por guardar um modo de tela cheia, largura e altura em pixels.
			 */
			class Mode {
			private:
				///Largura do modo.
				size_t _width;
				///Altura do modo.
				size_t _height;

			public:
				///Construtor padrão, inicializa largura e altura com zero.
				Mode():_width(0), _height(0) {}

				///Inicializa largura e altura com as passadas por parametro.
				Mode(size_t iwidth, size_t iheight):_width(iwidth), _height(iheight) {}

				///Construtor de cópia.
				Mode(const Mode &dm):_width(dm._width), _height(dm._height) {}

				///Obtém a largura do modo.
				size_t width() const {return this->_width;}

				///Obtém a altura do modo.
				size_t height() const {return this->_height;}

				///Operador de cópia.
				Mode &operator=(const Mode &b) {
					this->_width = b._width;
					this->_height = b._height;
					return *this;
				}

				///Operador de comparação para ordenar os modos de tela cheia.
				bool operator<(const Mode &b) const {return (_width > b._width) || (_width == b._width && _height > b._height);}

				friend class Display;
			};

			///Identificador do modo padrão.
			static const size_t DEFAULT;

		private:
			///Lista de modos de tela cheia.
			std::vector<Mode> _display_list;

			///Modo padrão, o mesmo utilizado pelo sistema operacional.
			Mode _default;

			///Modo selecionado.
			size_t _selected;

			///Indica se o modo de tela cheia está ativado.
			bool _active;

		public:
			///Inicializa os atributos do objeto e preenche a lista de modos.
			Display(int iselected):_display_list(), _default(0,0), _selected(iselected), _active(false) {
				update();
			}

			///Retorna para o modo padrão antes de destruir o objeto.
			~Display() {
				reset();
			}

			///Ativa o modo selecionado.
			void active();

			///Retorna para o modo padrão do sistema.
			void reset();

			/**
			 * Obtém um modo de tela cheia. Se o parametro for a constante DEFAULT, então o modo retornado é o mesmo
			 * utilizado pelo sitema operacional. Caso contrario ele deve ser menor que a quantidade total de modos.
			 *
			 * @param iid Identificador do modo que se deseja obter.
			 * @return O modo de tela cheia requisitado.
			 */
			Mode mode(size_t iid) {
				if(iid == Display::DEFAULT) {
					return this->_default;
				} else if(iid >= this->_display_list.size()) {
					base::log(base::Warning, "Display::mode() : Tentou obter um modo de display invalido: número %d de %d modos. Modo padrão retornado.", iid, this->_display_list.size());
					return this->_default;
				}
				return this->_display_list[iid];
			}

			/**
			 * Obtém a quantidade de modos de tela cheia disponíveis.
			 * @return
			 */
			size_t size() {
				return this->_display_list.size();
			}

			/**
			 * Este método é utilizado para selecionar o modo de tela cheia a ser utilizado pelo programa enquanto
			 * estiver em tela cheia.
			 *
			 * @param iid Identificador do modo desejado.
			 */
			void select(size_t iid) {
				if(iid != Display::DEFAULT && iid >= this->_display_list.size()) {
					base::log(base::Warning, "Display::mode() : Tentou selecionar um modo de display invalido: número %d de %d modos. Modo padrão selecionado.", iid, this->_display_list.size());
					iid = Display::DEFAULT;
				}
				this->_selected = iid;
				if(this->_active) {
					active();
				}
			}

			/**
			 * Obtém o identificador do modo de tela cheia selecionado.
			 *
			 * @return Identificador do modo selecionado.
			 */
			size_t selected() const {return this->_selected;}

			///Atualiza a lista de modos de tela cheia.
			void update();
		};
	}
}

#endif /* BOLT_CORE_DISPLAY_H_ */
