/*
 * Render.cpp
 *
 *  Created on: 18/01/2013
 *      Author: Douglas
 */

#include <core/Render.h>

#include <core/Win32.h>

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>

namespace bolt {
	namespace core {
		Render *Render::_instance = NULL;

		Render::Render(size_t iwindow_id)
		:_window_id(iwindow_id), _device_context(NULL), _opengl_context(NULL), _width(0), _height(0) {
			///Atualiza a instância atual e evita que mais de uma seja criada.
			if(_instance) {
				throw base::FatalException(base::print("Singleton<Render> : Tentou criar um novo objeto de um singleton!"));
			}
			_instance = this;

			Win32::createOpenGL(this->_window_id, &this->_device_context, &this->_opengl_context);
		}

		Render::~Render() {
			Win32::destroyOpenGL(this->_window_id, this->_device_context, this->_opengl_context);

			///Volta o ponteiro da instância atual para nulo após a atual ser deletada.
			_instance = NULL;
		}

		void Render::onResize(size_t iwidth, size_t iheight) {
			this->_width = iwidth;
			this->_height = iheight;
			for(size_t i=0 ; i<this->_listeners.size() ; i++) {
				this->_listeners[i]->onResize(iwidth, iheight);
			}
		}

		void Render::onUpdate() {
			for(size_t i=0 ; i<this->_listeners.size() ; i++) {
				this->_listeners[i]->onUpdate();
			}

			SwapBuffers((HDC)this->_device_context);
		}

		void Render::bind(Render::Listener *ilistener) {
			this->_listeners.push_back(ilistener);
			ilistener->onResize(this->_width, this->_height);
		}

		void Render::unbind(Render::Listener *ilistener) {
			std::vector<Render::Listener *>::iterator it;
			for(it=this->_listeners.begin() ; it!=this->_listeners.end() ; it++) {
				if((*it) == ilistener) {
					this->_listeners.erase(it);
					ilistener->_render = NULL;
					return;
				}
			}
		}

		void Render::origin(float iscale) {
			glBegin(GL_LINES); {

				glColor3f(1.0f, 0.0f, 0.0f);
				glVertex3f(0.0f, 0.0f, 0.0f);
				glVertex3f(iscale, 0.0f, 0.0f);

				glColor3f(0.0f, 1.0f, 0.0f);
				glVertex3f(0.0f, 0.0f, 0.0f);
				glVertex3f(0.0f, iscale, 0.0f);

				glColor3f(0.0f, 0.0f, 1.0f);
				glVertex3f(0.0f, 0.0f, 0.0f);
				glVertex3f(0.0f, 0.0f, iscale);

			} glEnd();
		}

		void Render::grid(size_t isize) {
			glBegin(GL_LINES); {

				glColor3f(0.5f, 0.5f, 0.5f);
				for(size_t i=0 ; i<=isize ; i++) {
					glVertex3f(float(isize), float(i), 0.0f);
					glVertex3f(-float(isize), float(i), 0.0f);
					glVertex3f(float(isize), -float(i), 0.0f);
					glVertex3f(-float(isize), -float(i), 0.0f);

					glVertex3f(float(i), float(isize), 0.0f);
					glVertex3f(float(i), -float(isize), 0.0f);
					glVertex3f(-float(i), float(isize), 0.0f);
					glVertex3f(-float(i), -float(isize), 0.0f);
				}

			} glEnd();
		}
	}  // namespace core
}  // namespace bolt


