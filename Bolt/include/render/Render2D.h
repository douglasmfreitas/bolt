/*
 * Render2D.h
 *
 *  Created on: 25/02/2013
 *      Author: Douglas
 */

#ifndef BOLT_RENDER_RENDER2D_H_
#define BOLT_RENDER_RENDER2D_H_

#include <render/Render.h>

#include <render/Framebuffer.h>

namespace bolt {
	namespace render {
		class Render2D {
		private:
			Framebuffer _frame;

		public:
			Render2D();
			~Render2D();

			void resize(size_t iwidth, size_t iheight);
		};
	}  // namespace render
}  // namespace bolt

#endif /* RENDER2D_H_ */
