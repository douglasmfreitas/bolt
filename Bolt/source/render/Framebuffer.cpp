/*
 * Framebuffer.cpp
 *
 *  Created on: 21/02/2013
 *      Author: Douglas
 */

#include <render/Framebuffer.h>

#include <GL/glew.h>
#include <GL/gl.h>

namespace bolt {
	namespace render {
		Framebuffer::Framebuffer(size_t iwidth, size_t iheight, NColorBuffers in_color_buffers)
		:_id(0), _width(iwidth), _height(iheight), _n_color_buffers(in_color_buffers) {
			create();
		}

		void Framebuffer::destroy() {
			glDeleteFramebuffers(1, &_id);
		}

		void Framebuffer::create() {
			glGenFramebuffers(1, &_id);
			bind();

			_color_buffer_0.loadColorBuffer(_width, _height);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, _color_buffer_0.type(), _color_buffer_0.id(), 0);

			if(_n_color_buffers >= NCB2) {
				_color_buffer_1.loadColorBuffer(_width, _height);
				glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, _color_buffer_1.type(), _color_buffer_1.id(), 0);
			}

			_depth_buffer.loadDepthBuffer(_width, _height);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, _depth_buffer.type(), _depth_buffer.id(), 0);

			if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
				throw RenderException("Framebuffer::Framebuffer : Não foi possivel criar o framebuffer. Formato invalido.");
			}

			unbind();
		}

		Framebuffer::~Framebuffer() {
			destroy();
		}

		void Framebuffer::bind() {
			glBindFramebuffer(GL_FRAMEBUFFER, _id);

			if(_n_color_buffers == NCB2) {
				GLuint attachments[2] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
				glDrawBuffers(2, attachments);
			}
		}

		void Framebuffer::unbind() {
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		}

		void Framebuffer::resize(size_t iwidth, size_t iheight) {
			destroy();
			_width = iwidth;
			_height = iheight;
			create();
		}
	}  // namespace render
}  // namespace bolt
