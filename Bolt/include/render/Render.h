/*
 * Render.h
 *
 *  Created on: 18/01/2013
 *      Author: Douglas
 */

#ifndef BOLT_RENDER_RENDER_H_
#define BOLT_RENDER_RENDER_H_

#include <base/Base.h>

namespace bolt {
	namespace render {
		class Texture;
		class Framebuffer;

		class Shader;

		class Render2D;
		class Render3D;

		/**
		 * Exceção a ser lançada pelos componentes desse namespace.
		 */
		class RenderException : public base::Exception {
		public:
			RenderException(std::string iwhat) : Exception(iwhat) {}
			virtual std::string what() const {return "RenderException : " + this->_what;}
		};
	}  // namespace render
}  // namespace bolt

#endif /* BOLT_RENDER_RENDER_H_ */
