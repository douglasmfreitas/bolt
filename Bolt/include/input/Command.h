/*
 * Command.h
 *
 *  Created on: 18/01/2013
 *      Author: Douglas
 */

#ifndef BOLT_INPUT_COMMAND_H_
#define BOLT_INPUT_COMMAND_H_

#include <input/Input.h>

namespace bolt {
	namespace input {
		/**
		 * Esta classe define um comando, que é composto por três estados: gatilho, botão e eixo.
		 *
		 * O gatilho é ativado toda vez que o comando é pressionado ou o eixo fica acima de um limite superior, mas não
		 * é desativado quando ele é solto ou o eixo fica abaixo do limite mínimo, ele só é desativado no inicio da
		 * atualização de quadro.
		 *
		 * O botão é semelhante ao gatilho, porém ele só é desativado quando o comando é solto ou o eixo fica abaixo
		 * do limite mínimo.
		 *
		 * Já eixo é utilizado para representar a entrada analógica, ele funciona do mesmo modo que o botão, porém
		 * utiliza 0.0f no lugar de false e 1.0f no lugar de true, e quando o comando é atualizado com a função #move()
		 * o eixo assume o valor que é passado por parametro para esta função.
		 *
		 * Para verificar cada um dos três, as funções t(), p() e a() são utilizadas para gatilho, botão e eixo
		 * respectivamente.
		 *
		 * @author Douglas M. Freitas
		 */
		class Command {
		private:
			///Estado do gatilho.
			bool _trigger;

			///Estado do botão.
			bool _button;

			///Estado do eixo.
			float _axis;

			///Pressiona o comando.
			void press() {_trigger = true; _button = true;}

			///Solta o comando.
			void release() {_button = false;}

			///Move o comando.
			void move(float iaxis) {
				if(this->_axis > iaxis && iaxis < 0.4f)	{release();}
				if(this->_axis < iaxis && iaxis > 0.5f)	{press();}
				this->_axis = iaxis;
			}

		public:
			///Inicializa o comando.
			Command():_trigger(false), _button(false), _axis(0.0f) {}

			/**
			 * Quando o comando é copiado ele não mantém os valores do original.
			 * @param ic Comando a ser copiado.
			 */
			Command(const Command &ic):_trigger(false), _button(false), _axis(0.0f) {}

			///Finaliza o comando.
			~Command() {}

			/**
			 * Obtém o estado do gatilho.
			 * @return Estado do gatilho: true = ativado, false = não ativado.
			 */
			bool t() const {return _trigger;}

			/**
			 * Obtém o estado do botão.
			 * @return Estado do botão: true = pressionado, false = não pressionado.
			 */
			bool p() const {return _button;}

			/**
			 * Obtém a posição do eixo.
			 * @return Posição do eixo: varia de 0.0f a 1.0f.
			 */
			float a() const {return _axis;}

			friend class CommandRow;
			friend class InputChart;
		};

		/**
		 * Esta classe é responsavel por agrupar os comandos base, cada um pertencendo a um dos quatro jogadores.
		 * Além disso ela atribui um nome aos comandos e uma descrição.
		 *
		 * Já que cada comando terá uma cópia para os 4 jogadores o primeiro nivel de agrupamento é o de jogadores,
		 * assim o nome é atribuido uma única vez para os 4 junto com a descrição.
		 *
		 * @author Douglas M. Freitas
		 */
		class CommandRow {
		private:
			///Vetor de comandos.
			Command _commands[4];

			///Nome do comando.
			std::string _name;

			///Descrição do comando.
			std::string _desc;

			///Função a ser chamada no inicio de cada atualização para desativar os gatilhos.
			void reset() {
				this->_commands[0]._trigger = false;
				this->_commands[1]._trigger = false;
				this->_commands[2]._trigger = false;
				this->_commands[3]._trigger = false;
			}

		public:
			/**
			 * Inicializa o comando.
			 *
			 * @param iname Nome do comando.
			 * @param idesc Descrição do comando.
			 */
			CommandRow(std::string iname, std::string idesc)
			:_name(iname), _desc(idesc) {
				this->_commands[0] = Command();
				this->_commands[1] = Command();
				this->_commands[2] = Command();
				this->_commands[3] = Command();
			}

			/**
			 * Copia o comando.
			 *
			 * @param icmd Comando a ser copiado.
			 */
			CommandRow(const CommandRow &icmd)
			:_name(icmd._name), _desc(icmd._desc) {
				this->_commands[0] = Command();
				this->_commands[1] = Command();
				this->_commands[2] = Command();
				this->_commands[3] = Command();
			}

			/**
			 * Finaliza o comando.
			 */
			~CommandRow() {}

			/**
			 * Obtém o nome do comando.
			 *
			 * @return Nome do comando.
			 */
			std::string name() const {return this->_name;}

			/**
			 * Obtém a descrição do comando.
			 *
			 * @return Descrição do comando.
			 */
			std::string desc() const {return this->_desc;}

			/**
			 * Obtém o comando do jogador passado por parametro que pode ser 0, 1, 2 ou 3. Nessa função não é feita a
			 * verificação se o que foi passado por parametro está certo.
			 *
			 * @param iplayer Identificador do jogador.
			 * @return Referência do comando.
			 */
			Command &operator[](size_t iplayer) {
				return _commands[iplayer];
			}

			friend class InputChart;
		};
	}  // namespace input
}  // namespace bolt


#endif /* BOLT_INPUT_COMMAND_H_ */
