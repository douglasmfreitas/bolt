/*
 * Render.h
 *
 *  Created on: 18/01/2013
 *      Author: Douglas
 */

#ifndef BOLT_BASE_RENDER_H_
#define BOLT_BASE_RENDER_H_

#include <core/Core.h>

#include <vector>

namespace bolt {
	namespace core {
		/**
		 * Esta classe tem o propósito de comandar o processo de renderização atravez de um Listener. O listener recebe
		 * o evento de atualização e de redimensionamento da janela. Além disso esta classe é responsavel por
		 * inicializar e finalizar o OpenGL. Com isso o núcleo se mantém livre de qualquer componente que seja
		 * construido sobre ele e continua tendo um certo controle.
		 *
		 * Basicamente o objetivo não é realizar a renderização em si, mas sim controlar o processo.
		 */
		class Render {
		public:
			/**
			 * Esta classe serve de base para qualquer classe que deseja receber os eventos do Render.
			 */
			class Listener {
			private:
				///Ponteiro para o render a qual o listener está vinculado.
				Render *_render;

			protected:

				/**Função chamada toda vez que a janela muda de tamanho.
				 * @param iwidth Largura da janela.
				 * @param iheight Altura da janela.*/
				virtual void onResize(size_t iwidth, size_t iheight) {};

				///Função chamda toda vez que a janela atualiza o seu conteúdo.
				virtual void onUpdate() {};

			public:
				///Inicializa o listener.
				Listener():_render(NULL) {}

				///Desvincula o objeto da entrada antes do objeto ser excluido.
				virtual ~Listener() {if(_render){_render->unbind(this);}}

				friend class Render;
			};

		private:
			///Identificador da janela. Utilizado para criar o contexto OpenGL e destruir o mesmo.
			size_t _window_id;

			///Contexto do dispositovo.
			void *_device_context;

			///Contexto OpenGL.
			void *_opengl_context;

			size_t _width;
			size_t _height;

			///Listeners vinculados ao render.
			std::vector<Render::Listener *> _listeners;

			/**
			 * Função chamada toda vez que a janela muda de tamanho.
			 * @param iwidth Largura da janela.
			 * @param iheight Altura da janela.
			 */
			void onResize(size_t iwidth, size_t iheight);

			/**
			 * Função chamda toda vez que a janela atualiza o seu conteúdo.
			 * Além disso ela repassa o evento para todos os listeners.
			 */
			void onUpdate();

			///Ponteiro para a instância atual do singleton.
			static Render *_instance;

			///Impede que cópias do signleton sejam feitas.
			Render(const Render &);

		public:
			/**
			 * Inicializa o OpenGL.
			 *
			 * @param iwindow_id Identificador da janela.
			 */
			Render(size_t iwindow_id);

			/**
			 * Finaliza o OpenGL.
			 */
			~Render();

			/**
			 * Vincula o listener à entrada.
			 * @param ilistener Listener a ser vinculado.
			 */
			void bind(Render::Listener *ilistener);

			/**
			 * Desregistra o listener passado por parametro.
			 * @param ilistener Listener a ser desregistrado.
			 */
			void unbind(Render::Listener *ilistener);

			size_t width() const {return this->_width;}
			size_t height() const {return this->_height;}

			/**
			 * Desenha na tela a origem na forma de 3 vetores com cores diferentes, um para cada eixo.
			 *
			 * @param iscale Escala da origem.
			 */
			static void origin(float iscale);

			/**
			 * Desenha na tela uma grade no centro em volta da origem nos eixos x e y.
			 *
			 * @param isize Raio da grade.
			 */
			static void grid(size_t isize);

			///Este método obtém a instância atual do singleton.
			static Render *get() {
				return _instance;
			}

			friend Window;
		};
	}  // namespace core
}  // namespace bolt


#endif /* BOLT_BASE_RENDER_H_ */
