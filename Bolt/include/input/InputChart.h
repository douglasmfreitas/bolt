/*
 * InputChart.h
 *
 *  Created on: 19/01/2013
 *      Author: Douglas
 */

#ifndef BOLT_INPUT_INPUTSHEET_H_
#define BOLT_INPUT_INPUTSHEET_H_

#include <input/Input.h>
#include <input/Command.h>
#include <core/Input.h>

#include <string>
#include <map>

namespace bolt {
	namespace input {
		/**
		 * Classe responsavel por agrupar os comandos de entrada, além disso ela recebe os eventos de entrada da classe
		 * bolt::core::Input e os repassa para os comandos.
		 *
		 * @author Douglas M. Freitas
		 */
		class InputChart : public core::Input::Listener {
		private:
			///Tipo do mapa de comandos.
			typedef std::map<std::string, CommandRow> CmdMap;

			///Mapa de comandos.
			CmdMap _commands;

			///Mapa de vinculações entre comandos e teclas.
			std::map<core::Input::KeyCode, Command*> _bindings;

			///Nome da tabela de entrada.
			std::string _name;

			///Descrição da tabela de entrada.
			std::string _desc;

			/**
			 * Função chamada toda vez que uma tecla é pressionada.
			 *
			 * @param ikey Tecla pressionada.
			 */
			virtual void onKeyDown(core::Input::KeyCode ikey) {
				std::map<core::Input::KeyCode, Command*>::iterator it = this->_bindings.find(ikey);
				if(it != this->_bindings.end()) {
					it->second->press();
				}
			}

			/**
			 * Função chamada toda vez que uma tecla é solta.
			 *
			 * @param ikey Tecla solta.
			 */
			virtual void onKeyUp(core::Input::KeyCode ikey) {
				std::map<core::Input::KeyCode, Command*>::iterator it = this->_bindings.find(ikey);
				if(it != this->_bindings.end()) {
					it->second->release();
				}
			}

			/**
			 * Função chamada toda vez que uma tecla se move.
			 *
			 * @param iaxis Tecla que se moveu.
			 * @param ipos Posição da tecla.
			 */
			virtual void onKeyMove(core::Input::KeyCode iaxis, float ipos) {
				std::map<core::Input::KeyCode, Command*>::iterator it = this->_bindings.find(iaxis);
				if(it != this->_bindings.end()) {
					it->second->move(ipos);
				}
			}

			/**
			 * Função chamda toda vez que a janela atualiza o seu conteúdo.
			 */
			virtual void onUpdate() {
				CmdMap::iterator it;
				for(it = this->_commands.begin() ; it!=this->_commands.end() ; it++) {
					it->second.reset();
				}
			}

		public:
			/**
			 * Inicializa a tabela de entrada.
			 *
			 * @param iname Nome da tabela.
			 * @param idesc Descrição da tabela.
			 */
			InputChart(std::string iname, std::string idesc);

			///finaliza a tabela de entrada.
			virtual ~InputChart();

			/**
			 * Vincula a tecla passada por parametro com o comando e jogador indicados. Caso ela esteja vinculada a
			 * outro comando, a tecla é desvinculada do anterior.
			 *
			 * @param ikey Tecla a ser vinculada.
			 * @param icmd Comando a ser utilizado.
			 * @param iplayer Jogador a ser utilizado.
			 */
			void bind(core::Input::KeyCode ikey, std::string icmd, size_t iplayer);

			/**
			 * Desvincula a tecla passada por parametro do comando ao qual ela está atribuida. Caso nenhum comando
			 * esteja atribuido a ela nada acontece.
			 *
			 * @param ikey
			 */
			void unbind(core::Input::KeyCode ikey);

			/**
			 * Adiciona um comando à tabela.
			 *
			 * @param iid Identificador do comando.
			 * @param iname Nome do comando.
			 * @param idesc Descrição do comando.
			 */
			void addCommand(std::string iid, std::string iname, std::string idesc);

			/**
			 * Obtém a quantidade de comandos da tabela.
			 *
			 * @return
			 */
			size_t size() {
				return this->_commands.size();
			}

			/**
			 * Obtém um comando de entrada de acordo com a posição dele, onde a ordem é definida pelo identificador.
			 * Este método deve ser utilizado quando se deseja obter todos os comandos.
			 *
			 * @param iid Posição do comando de entrada.
			 * @return Comando de entrada referênte à posição.
			 */
			CommandRow &operator[](size_t iid) {
				if(iid >= this->_commands.size()) {
					throw InputException(base::print("InputChart::operator[] : Existe nenhum comando de entrada com o identificador \"%d\".", iid));
				}

				CmdMap::iterator it = this->_commands.begin();
				for(size_t i=0 ; i<iid ; i++, it++);
				return (it->second);
			}

			/**
			 * Obtém o comando de entrada associado ao identificador.
			 *
			 * @param iid Identificador do comando desejado.
			 * @return identificador do comando.
			 */
			CommandRow &operator[](std::string iid) {
				CmdMap::iterator it = this->_commands.find(iid);
				if(it != this->_commands.end()) {
					return (it->second);
				} else {
					throw InputException(base::print("InputChart::operator[] : Existe nenhum comando de entrada com o identificador \"%s\".", iid.c_str()));
				}
			}

			///Ativa o recebimento de mensagens de entrada.
			void activate();

			///Desativa o recebimento de mensagens de entrada.
			void deactivate();
		};
	}  // namespace input
}  // namespace bolt

#endif /* BOLT_INPUT_INPUTSHEET_H_ */
