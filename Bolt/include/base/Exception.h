/*
 * Exception.h
 *
 *  Created on: 13/12/2012
 *      Author: Douglas M. Freitas
 */

#ifndef BOLT_BASE_EXCEPTION_H_
#define BOLT_BASE_EXCEPTION_H_

#include <string>

namespace bolt {
	namespace base {

		/**
		 * Classe base para as exceções. Toda exceção deriva dessa classe e todas as exceções lançadas deve ser
		 * um objeto dessa classe ou de uma derivada dela.
		 */
		class Exception {
		protected:
			///Motivo da exceção.
			std::string _what;

		public:
			/**
			 * Construtor da exceção, deve receber o motivo da mesma por parametro.
			 * @param iwhat Motivo da exceção.
			 */
			Exception(std::string iwhat) : _what(iwhat) {}

			/**
			 * Destrutor da exceção.
			 */
			virtual ~Exception() {}

			/**
			 * Retorna o motivo da exceção precedido por seu tipo.
			 * @return O motivo da exceção precedido por seu tipo.
			 */
			virtual std::string what() const {return "Exception : " + this->_what;}
		};

		/**
		 * Exceção fatal. Esta é lançada quando uma falha
		 */
		class FatalException : public Exception {
		public:
			FatalException(std::string iwhat) : Exception(iwhat) {}
			virtual std::string what() const {return "FatalException : " + this->_what;}
		};

	}  // namespace base
}  // namespace bolt

#endif /* BOLT_BASE_EXCEPTION_H_ */
