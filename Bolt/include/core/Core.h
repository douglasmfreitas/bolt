/*
 * Core.h
 *
 *  Created on: 19/12/2012
 *      Author: Douglas M. Freitas
 */

#ifndef BOLT_CORE_CORE_H_
#define BOLT_CORE_CORE_H_

#include <base/Base.h>

namespace bolt {
	/**
	 * Este namespace é composto por todos os componentes essenciais para o funcionamento do programa, como a janela.
	 * Ele contém somente o necessário para o funcionamento mínimo do sistema, qualquer funcionalidade mais específica
	 * está disponivel em outro namespace.
	 *
	 * @author Douglas M. Freitas
	 */
	namespace core {

		class Display;
		class Window;
		class Input;
		class Render;

		/**
		 * Exceção a ser lançada pelos componentes desse namespace.
		 */
		class CoreException : public base::Exception {
		public:
			CoreException(std::string iwhat) : Exception(iwhat) {}
			virtual std::string what() const {return "CoreException : " + this->_what;}
		};
	}  // namespace core
}  // namespace bolt

#endif /* BOLT_CORE_CORE_H_ */
