/*
 * LogFile.cpp
 *
 *  Created on: 19/12/2012
 *      Author: Douglas M. Freitas
 */

#include <base/LogFile.h>

#include <fstream>
#include <iostream>
#include <ctime>
#include <cwchar>

namespace bolt {
	namespace base {

		LogFile::LogFile(std::string ifilename, std::string ititle, struct tm *itimeinfo)
		:_filename(ifilename) {
			std::ofstream out(this->_filename.c_str(), std::ios::trunc);
			if(out.is_open()) {

				char buffer[50];
				strftime(buffer, 50, "%d/%m/%Y %H:%M:%S - Log: ", itimeinfo);

				ititle = buffer + ititle + " ";

				std::string title = "-- -----------------------------------------------------------------------------";
				title.replace(3, ititle.size(), ititle);

				out << "--------------------------------------------------------------------------------" << std::endl;
				out << title << std::endl;
				out << "--------------------------------------------------------------------------------" << std::endl;
				out << std::endl;

				std::cout << "--------------------------------------------------------------------------------" << std::endl;
				std::cout << title << std::endl;
				std::cout << "--------------------------------------------------------------------------------" << std::endl;
				std::cout << std::endl;

			} else {
				throw LogException(print("LogFile::LogFile() : Não foi possível abrir o arquivo de log: %s.", this->_filename.c_str()));
			}
			out.close();
		}

		LogFile::~LogFile() {
			std::ofstream out(this->_filename.c_str(), std::ios::app);
			if(out.is_open()) {

				out << std::endl;
				out << "--------------------------------------------------------------------------------" << std::endl;

				std::cout << std::endl;
				std::cout << "--------------------------------------------------------------------------------" << std::endl;

			} else {
				throw LogException(print("LogFile::LogFile() : Não foi possível abrir o arquivo de log: %s.", this->_filename.c_str()));
			}
			out.close();
		}

		void LogFile::write(LogType itype, struct tm *itimeinfo, std::string iline) {
			std::ofstream out(this->_filename.c_str(), std::ios::app);
			if(out.is_open()) {

				char buffer[50];
				strftime(buffer, 50, "%H:%M:%S", itimeinfo);

				switch (itype) {
					case Error:
						out << buffer << " - Error:   " << iline << std::endl;
						std::cout << buffer << " - Error:   " << iline << std::endl;
						break;
					case Warning:
						out << buffer << " - Warning: " << iline << std::endl;
						std::cout << buffer << " - Warning: " << iline << std::endl;
						break;
					case Info:
						out << buffer << " - Info:    " << iline << std::endl;
						std::cout << buffer << " - Info:    " << iline << std::endl;
						break;
					case Nothing:
						out << buffer << " - Nothing: " << iline << std::endl;
						std::cout << buffer << " - Nothing: " << iline << std::endl;
						break;
					default:
						break;
				}

			} else {
				throw LogException(print("LogFile::LogFile() : Não foi possível abrir o arquivo de log: %s.", this->_filename.c_str()));
			}
			out.close();
		}

	}  // namespace base
}  // namespace bolt


