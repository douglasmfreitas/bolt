/*
 * Shader.cpp
 *
 *  Created on: 22/02/2013
 *      Author: Douglas
 */

#include <render/Shader.h>

#include <GL/glew.h>
#include <GL/gl.h>

#include <fstream>

namespace bolt {
	namespace render {
		Shader::Shader(std::string ivert, std::string ifrag)
		:_program_id(0), _vert_id(0), _frag_id(0){
			load(ivert, ifrag);
		}
		Shader::~Shader() {
			unload();
		}

		void Shader::log(std::string ivert, std::string ifrag) {
		    int infolog_length = 0;
		    std::string info_log_vert;
		    std::string info_log_frag;
		    std::string info_log_program;

			glGetShaderiv(_vert_id, GL_INFO_LOG_LENGTH,&infolog_length);
		    if (infolog_length > 0) {
			    int chars_written = 0;
		    	char *info_log = new char [infolog_length];
		        glGetShaderInfoLog(_vert_id, infolog_length, &chars_written, info_log);
		        info_log_vert = info_log;
		        delete [] info_log;
		    }

			glGetShaderiv(_frag_id, GL_INFO_LOG_LENGTH,&infolog_length);
		    if (infolog_length > 0) {
			    int chars_written = 0;
		    	char *info_log = new char [infolog_length];
		        glGetShaderInfoLog(_frag_id, infolog_length, &chars_written, info_log);
		        info_log_frag = info_log;
		        delete [] info_log;
		    }

		    glGetProgramiv(_program_id, GL_INFO_LOG_LENGTH,&infolog_length);
		    if (infolog_length > 0) {
			    int chars_written = 0;
		    	char *info_log = new char [infolog_length];
		    	glGetProgramInfoLog(_program_id, infolog_length, &chars_written, info_log);
		        info_log_program = info_log;
		        delete [] info_log;
		    }

			base::log(base::Info, "Shader::logShader : InfoLog do shader:"
					"\n----------------------------------------"
					"\n-- Vertex Shader: '%s'\n%s"
					"\n-- Fragment Shader: '%s'\n%s"
					"\n\n-- Program:\n%s"
					"\n----------------------------------------"
					, ivert.c_str(), info_log_vert.c_str(), ifrag.c_str(), info_log_frag.c_str(), info_log_program.c_str());
		}

		void Shader::bind() {
			glUseProgram(_program_id);
		}

		void Shader::unbind() {
			glUseProgram(0);
		}

		void Shader::load(std::string ivert, std::string ifrag) {
			char *vert_source;
			size_t vert_source_length;

			std::ifstream vin(ivert.c_str(), std::ios::binary);
			if(vin.is_open()) {
				vin.seekg(0, std::ios::end);
				vert_source_length = vin.tellg();
				vin.seekg(0, std::ios::beg);

				vert_source = new char[vert_source_length];
				vin.read(vert_source, vert_source_length);
			} else {
				throw RenderException(base::print("Shader::load : Não foi possivel abrir o arquivo '%s' contendo o código fonte do shader de vértices.", ivert.c_str()));
			}

			char * frag_source;
			size_t frag_source_length;

			std::ifstream fin(ifrag.c_str(), std::ios::binary);
			if(fin.is_open()) {
				fin.seekg(0, std::ios::end);
				frag_source_length = fin.tellg();
				fin.seekg(0, std::ios::beg);

				frag_source = new char[frag_source_length];
				fin.read(frag_source, frag_source_length);
			} else {
				delete [] frag_source;
				throw RenderException(base::print("Shader::load : Não foi possivel abrir o arquivo '%s' contendo o código fonte do shader de vértices.", ivert.c_str()));
			}

			_vert_id = glCreateShader(GL_VERTEX_SHADER);
			glShaderSource(_vert_id, 1, (const GLchar **)&vert_source, (const GLint *)&vert_source_length);
			glCompileShader(_vert_id);

			delete [] vert_source;

			_frag_id = glCreateShader(GL_FRAGMENT_SHADER);
			glShaderSource(_frag_id, 1, (const GLchar **)&frag_source, (const GLint *)&frag_source_length);
			glCompileShader(_frag_id);

			delete [] frag_source;

			_program_id = glCreateProgram();
			glAttachShader(_program_id, _vert_id);
			glAttachShader(_program_id, _frag_id);

			glLinkProgram(_program_id);

			log(ivert, ifrag);
		}

		void Shader::unload() {
			if(_program_id) {
				glDetachShader(_program_id, _vert_id);
				glDetachShader(_program_id, _frag_id);

				glDeleteShader(_vert_id);
				glDeleteShader(_frag_id);
				glDeleteProgram(_program_id);

				_program_id = 0;
				_vert_id = 0;
				_frag_id = 0;
			}
		}
	}  // namespace render
}  // namespace bolt
